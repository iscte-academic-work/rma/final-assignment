#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/common/transforms.h>
#include <pcl/common/eigen.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/search/kdtree.h>	
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/common.h>

#include <pcl/segmentation/extract_clusters.h>

#include <algorithm>
#include <sstream>
#include <fstream>  
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector> 
#include <iostream>
#include <math.h>   


/* This class is mainly responsible for the first two requirements from the assignment. Provides helper
*  functions to serve the need for detection
*/
class DetectionHelper{
public:
    // ---------- Global vars and constans ------------------- 
    const char* CLOUD_OUTPUT_BASE_FOLDER = "../Out/";
    std::string CloudInputFolder = "."; 
    std::string BaseCloudsFolder = "."; // base folder for all clouds
    std::string CloudOutputFolder = ".";
    std::string CloudInAnalysis = ""; //without the extension!
    static const double robotHeight;
    static const double robotWidth;
    static const double robotHeightLowerTolerance;
    static const double robotWidthLowerTolerance;

    static const double robotHeightHigherTolerance;
    static const double robotWidthHigherTolerance;


    //--------------------------------------------------------

    //---------------Structs--------------

    enum ObjectClassification { WALL=0, ROBOT=1, OTHER=2, VOLUMETRIC_MARKER=3, LASER=4};
    struct ClassifiedObject{
        std::string name;
        ObjectClassification classification;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud;
        pcl::PointXYZRGBA minPt;
        pcl::PointXYZRGBA maxPt;
        pcl::PointXYZRGBA centerOfMassAndMeanColor;
        //This will be used only for ROBOT, and will be of type LASER or VOLUMETRIC_MARKER
        std::vector<ClassifiedObject> childObjects;
        double width;
        double height;
    };

    //--------------------------------------------------------

    //--------------Constructor and destructor
    DetectionHelper();
    ~DetectionHelper();

    //--------------------------------------------------------


    static void easyLog(std::string msg);
    static bool isRobot(double objectWidth, double objectHeight);
    static bool isPotentialRobot(double objectWidth);
    static double distanceBetweenPoints(pcl::PointXYZRGBA point1, pcl::PointXYZRGBA point2);
    static double distanceBetweenPointsOpenGL(pcl::PointXYZRGBA point1, pcl::PointXYZRGBA point2);
    static pcl::PointXYZRGBA scalePoint(pcl::PointXYZRGBA pt, float scaleFactor){
        pt.x *= scaleFactor;
        pt.y *= scaleFactor;
        pt.z *= scaleFactor;
        return pt;
    }

    static pcl::PointXYZRGBA sumPoints(pcl::PointXYZRGBA pt1, pcl::PointXYZRGBA pt2){
        pcl::PointXYZRGBA ptr;
        ptr.x=pt1.x+pt2.x;
        ptr.y=pt1.y+pt2.y;
        ptr.z=pt1.z+pt2.z;
        return ptr;
    }

    static void logMinMaxCenter(pcl::PointXYZRGBA minPt, pcl::PointXYZRGBA maxPt, pcl::PointXYZRGBA centerPt);
    void logCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, std::string cloudName, std::string description);
    void copyToCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_out, pcl::PointIndices indexes);
    
    void projectCloudToXZ(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_out);
    bool cloudWithinBoundingCylinder(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA centerOfMass, double radius, 
                                                    double height, double radiusTolerance, double minProportion, bool verbose);
    // Finds the farthest point from given point
    static pcl::PointXYZRGBA findMostDistantPoint(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA point);                                                    
    double findXoZMaxDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, std::string projectedCloudName, int numSearchIterations);
    double findXoZMaxDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr top_surface_cloud, std::string projectedCloudName, int numSearchIterations, bool extended, double minProportion, bool &shiftedCM);
    bool belongsToPlane(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, double a, double b, double c, double d, double tolerance, float minProportion);
    void normalizePlaneCoeffs(pcl::ModelCoefficients::Ptr coefficients, double *a, double *b, double *c, double *d);
    void createExecutionOutputFolder();
    void createCloudOutputFolder(std::string cloudName);
    int getdir (std::string dir, std::vector<std::string> &files, std::string mustContainString);
    void voxeliseCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in,
                        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised);
    float estimateStdDevDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, int Kneighbors);
    void estimateCenterOfMass(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA &centerOfMass);

    void getDominantPlane(pcl::SACSegmentation<pcl::PointXYZRGBA> seg, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers);
    void getDominantPlane(pcl::SACSegmentation<pcl::PointXYZRGBA> seg, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers, double distanceThreshold);
    void estimateCameraPose(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, double *pitch, double *roll, double *height);
    void rotatePointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in,
                        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated, 
                        double camera_pitch, double camera_roll, double camera_height);
    void cleanPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clean);
    void extractCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_extracted, pcl::PointIndices::Ptr inliers, bool negative);
    void getPointsAboveFloor(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_above_floor, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane);
    void mergeClouds(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud1, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud2);
    std::vector<ClassifiedObject>  extractObjectsFromCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in);
    int loadCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, std::string cloudName);

    std::vector<ClassifiedObject> extractRobotTopElements(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr robotCloud, int robotNumber);

};