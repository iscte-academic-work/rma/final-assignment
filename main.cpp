/*
 * Mixed Reality and Applications
 *
 * Abel Tenera, Gilberto Junior, Samatha Cane
 *  2019-2020 ISCTE-IUL
 *
 *  Assumed frames of reference:
 *  - PCL: z-blue (into the scene), x-red (to the right), y-green (downwards)
 *  - OSG: z (upwards), x (to the left), y (out of the scene)
 *
 */


#include <osg/Camera>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>
#include <osg/Texture2D>
#include <osg/MatrixTransform>
#include <osg/GraphicsContext>
#include <osg/Depth>
#include <osg/Material>
#include <osg/Texture2D>
#include <osg/PositionAttitudeTransform>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/AlphaFunc>
#include <osgGA/GUIEventHandler>
#include <osg/ShapeDrawable>
 


//#include "DetectionHelper.h"
#include "ARHelper.h"

int main(int argsc, char** argsv){ 
    DetectionHelper* detectionHelper = new DetectionHelper();
    
    // Create an output folder to load all clouds
    detectionHelper->createExecutionOutputFolder();
    std::cout<< "This execution will output everything to " << detectionHelper->CloudOutputFolder << std::endl;
    
    // Reads cloud input folder
    detectionHelper->CloudInputFolder = argsv[1]; //We are assuming that will have "/" in the end
    std::vector<std::string> files = std::vector<std::string>();

    // Gets dir content from input folder and writes to files vector. From now on we are assuming that everything assumes the format cloud_[number].pcd
    detectionHelper->getdir(detectionHelper->CloudInputFolder,files, ".pcd");
    // The next code just logs the content got
    std::cout<< "Input folder " << detectionHelper->CloudInputFolder << " contains:" << std::endl;
    for (unsigned int i = 0;i < files.size();i++) {
        cout << "\t"<< files[i] << endl;
    }

    // Since we are assuming a certain format, we just need the file count and their base directory to get the files
    // then in a further cicle we just build the file name based on "cloud_[i]"
    unsigned int numberOfFiles = files.size();
  
    // iterates over the cloud files
    double camera_pitch, camera_roll, camera_height;

    // Helper for AR
    ARHelper * arHelper ;

    //for(unsigned int i=0;i<numberOfFiles; i++){ // Uncomment this latter on    
    for(unsigned int i=0;i<numberOfFiles; i++){
        // Loads the cloud for this iteration
        std::string cloudFileName = "cloud_" + std::to_string(i);
        
        // Gets the unmodified cloud
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in (new pcl::PointCloud<pcl::PointXYZRGBA>); // gets the original cloud
        if(detectionHelper->loadCloud(cloud_in, cloudFileName) == -1 ){
            return -1;
        }

        //The first cloud is "empty" (just floor and wall), so we are using it to estimate the pose
        if(i==0){
            std::cout<< "Going to estimate camera pose from" << detectionHelper->CloudInAnalysis <<std::endl;
            // Estimates the values of pitch, roll and height
            detectionHelper->estimateCameraPose(cloud_in, &camera_pitch, &camera_roll, &camera_height);

            // Homogenize the cloud density with a voxelization of it. By reducing the number of pixels
            // We also reduce the processor charge
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised (new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->voxeliseCloud(cloud_in, cloud_voxelised);

            // Creates a rotated and voxelised version of the rotated cloud
            // Since we are not going to do nothing with the first cloud, this is just for logging purposes
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated (new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->rotatePointCloud(cloud_voxelised, cloud_rotated, camera_pitch, camera_roll, camera_height);

            //creates instance of ARHelper
            arHelper = new ARHelper(camera_pitch, camera_roll, camera_height);
        }
        else{
            // Homogenize the cloud density with a voxelization of it. By reducing the number of pixels
            // We also reduce the processor charge
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised (new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->voxeliseCloud(cloud_in, cloud_voxelised);

            // Rotate point cloud
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated (new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->rotatePointCloud(cloud_voxelised, cloud_rotated, camera_pitch, camera_roll, camera_height);

            // Remove the outliers
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clean (new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->cleanPointCloud(cloud_rotated,cloud_clean);


            // TODO: We have a bug when moving to sequence 2. We are detecting the wrong floor in here.
            //       Two possible solutions:
            //                     - Cut the cloud into
            //                     - When we estimate the camera pose, we use ransac and get floor coeffs. We can use those to validate
            //                       the cloud on plane obtained againts those. In that case we search for the next floor. Then we consider
            //                       the validated cloud points as inliers, and then separate everything
            //Gets the cloud above floor, and the cloud containing the floor
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_above_floor (new pcl::PointCloud<pcl::PointXYZRGBA>);
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
            detectionHelper->getPointsAboveFloor(cloud_clean,cloud_above_floor,cloud_on_plane);

            // Extracts objects on cloud by applying a classification process
            std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = detectionHelper->extractObjectsFromCloud(cloud_above_floor); // TODO: Estes objectos deverão ser apresentados coloridos!
             // At this point we have the objects classified
            // Engage the AR4RGBD :D
            arHelper->setCurrentCloud(cloud_in, cloud_clean,classifiedObjects);
            // There is trap here! We must invert z and probably y :D
            arHelper->runARExperiment();            
        }
    }
    
}

