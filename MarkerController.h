
#include <osgGA/GUIEventHandler>
#include <osg/PositionAttitudeTransform>

enum ARImageModes { NORMAL=0, RISK=1, SHADING=2};

class MarkerController : public osgGA::GUIEventHandler
{
public:
    MarkerController( osg::PositionAttitudeTransform* node, bool *_collidedLeft, bool *_collidedRight,
                   bool *_collidedFront, bool *_collidedBack, bool *_collidedBelow, ARImageModes *_imageMode)
    : _marker(node), collidedLeft(_collidedLeft),
    collidedRight(_collidedRight), collidedFront(_collidedFront), collidedBack(_collidedBack), collidedBelow(_collidedBelow), imageMode(_imageMode) {
    }
    virtual bool handle( const osgGA::GUIEventAdapter& ea,
                        osgGA::GUIActionAdapter& aa );
                        
    bool acquireMovement(){
        bool mov = moved;
        moved=false;
        return mov;
    }
    void setMoved(bool state){
        moved=state;
    }

    void setModeChanged(bool state){
        modeChanged=state;
    }

    // Returns if mode changed and resets it
    bool acquireModeChanged(){
        bool changed = modeChanged;
        modeChanged = false;
        return changed;
    }


protected:
    osg::ref_ptr<osg::PositionAttitudeTransform> _marker;
    bool *collidedLeft, *collidedRight, *collidedFront, *collidedBack, *collidedBelow;
    ARImageModes *imageMode;
    bool toogleCollisions=true;
    bool modeChanged=false, moved =false;
};


class MarkerCallback : public osg::NodeCallback
{
public:
    MarkerCallback(osg::PositionAttitudeTransform *_shadowTransf) :
    shadowTransf(_shadowTransf){}

    virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
protected:
    osg::PositionAttitudeTransform* shadowTransf;
};