#include "ARHelper.h"

// ----------STATIC------------------
// This shaders were got from e-leaning RMA page

const char* ARHelper::phongVertexSource = {
    "varying vec3 varnormal;\n"
    "varying vec4 varposition;\n"
    "void main()\n"
    "{\n"
    "varnormal = normalize(gl_NormalMatrix * gl_Normal);\n"
    "varposition =  gl_ModelViewMatrix * (gl_Vertex);\n"
    "gl_Position = gl_ModelViewProjectionMatrix * (gl_Vertex);\n"
    "}\n"
};
const char* ARHelper::phongFragmentSource = {
    "varying vec3 varnormal;\n"
    "varying vec4 varposition;\n"
    "void main(void){\n"
        "vec3 normal, lightDir, viewVector, halfVector;\n"
        "vec4 diffuse, ambient, globalAmbient, specular;\n"
        "float NdotL,NdotHV;\n"
        "normal = normalize(varnormal);   \n"
        "lightDir = normalize(vec3(gl_LightSource[0].position.xyz - varposition.xyz)); \n"
        "NdotL = max(dot(normal, lightDir), 0.0);        \n"
        "diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;  \n"
        "ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;  \n"
        "NdotHV = max(dot(normal, normalize(gl_LightSource[0].halfVector.xyz)),0.0);      \n"
        "specular = gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV,gl_FrontMaterial.shininess);  \n"
        "gl_FragColor = clamp(NdotL * diffuse + ambient + specular, 0.0, 1.0);\n"
        "gl_FragDepth = (1.0/gl_FragCoord.w)/1000.0;\n"
    "}\n"
};

const char* ARHelper::textureVertexSource = {
    "varying vec3 normal;\n"
    "void main()\n"
    "{\n"
    "    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;\n"
    "}\n"
};

const char* ARHelper::textureFramentSource = {
    "uniform sampler2D texture;\n"
    "uniform sampler2D textureDepth;\n"
    "void main()\n"
    "{\n"
    "   vec2 uv = vec2(gl_FragCoord.x/640.0, gl_FragCoord.y/480.0);\n"
    "   gl_FragColor = texture2D(texture, uv);\n"
    "   gl_FragDepth = (texture2D(textureDepth, uv)[2]);\n"
    "}\n"
};

const char* ARHelper::markerVertexSource = {
    "void main()\n"
    "{\n"
    "    gl_TexCoord[0] = gl_MultiTexCoord0;\n"
    "    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;\n"
    "}\n"
};
 
const char* ARHelper::markerFragmentSource = {
    "uniform sampler2D texture;\n"
    "void main()\n"
    "{\n"
    "    gl_FragColor = texture2D(texture, gl_TexCoord[0].st);\n"
    "    gl_FragDepth = (1.0/gl_FragCoord.w)/1000.0;\n"
    "}\n"
};

// --------------------------------------


// -----------------PUBLIC-----------------

ARHelper::ARHelper(double pitch, double roll, double height){


    this->pitch = pitch;
    this->roll = roll;
    this->height = height;
    distanceTextHudFrame = new osgText::Text;

    // Transform that allows marker to move in space
    markerTransf = new osg::PositionAttitudeTransform;
    shadowTransf = new osg::PositionAttitudeTransform;

    // Creates marker
    CreateMarker(markerTransf,shadowTransf);

    // Creates a controller for user to control a marker and image modes
    ctrler =  new MarkerController( markerTransf.get(), &collidedLeft, &collidedRight, &collidedFront, &collidedBack, &collidedBelow, &arImageMode);


    //Creates the base geode that will serve as a ruler tape
    ruler = createTextureGeode(rulerWidth, rulerLength, "../Resources/ruler.png" );

}

ARHelper::~ARHelper(){
    delete [] data;
    delete [] dataDepth;
    delete [] dataRiskColor;
}

void ARHelper::setCurrentCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr _cloud, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr clean_cloud, std::vector<DetectionHelper::ClassifiedObject> classifiedObjects){
    //First we initialize the obvious
    this->cloud = _cloud; 
    this->clean_cloud = clean_cloud;
    this->classifiedObjectsBuffer.push_back(classifiedObjects);

    //Compute the data arrays
    // The size of the data arrays is given by the product of cloud dimensions times 3. 
    //We multiply by 3, because it is the size of each channel group (x,y,z) and (r,g,b)
    int size = this->cloud->height*cloud->width*3; 
    // Allocates memory for each of the vectors
    this->data = (unsigned char*)calloc(size,sizeof(unsigned char));
    this->dataDepth = (unsigned char*)calloc(size,sizeof(unsigned char));
    this->dataRiskColor = (unsigned char*)calloc(size,sizeof(unsigned char));
    this->dataShadingFromRobot = (unsigned char*)calloc(size,sizeof(unsigned char));

    // Initializes the image arrays
    extractPointCloudColorDepthData(this->cloud, this->data, this->dataDepth);

    extractPointCloudColorCollisionRiskData(this->dataRiskColor);
    extractPointCloudColorShadingFromRobot(this->dataShadingFromRobot);

}


// Kind of the main function in here
void ARHelper::runARExperiment(){
    // This is the root node for the scene graph, that will contain the hierarchy of
    // cameras, ortho plane, and objects we may add in scene
    osg::ref_ptr<osg::Group> root;

    // Initializes the root node
    root = new osg::Group;

    // Initializes the cameras
    orthoCam = new osg::Camera;
    perspectiveCam = new osg::Camera;

    // Creates the ortho text
    osg::Image *image = new osg::Image; // Saves the image object for later to comute between modes
    osg::ref_ptr<osg::Geode> orthoTextureGeode = new osg::Geode;
    createOrthoTexture(orthoTextureGeode, image, data, dataDepth, cloud->width, cloud->height);

    //createOrthoTexture(orthoTextureGeode, data, dataDepth, cloud->width, cloud->height);
    
    // Sets the ortho and perspective cams, and adds the ortho tex to orthoCam
    setVirtualCameras(orthoCam, perspectiveCam, orthoTextureGeode);

    // Add cams to root geode hierarchy
    root->addChild(orthoCam.get());
    root->addChild(perspectiveCam.get());                   

    // Loads the arrows on the top of the object    
    osg::ref_ptr<osg::PositionAttitudeTransform> arrows_group_trf = loadArrowsOnObjects();

    perspectiveCam->addChild( arrows_group_trf );

    // Loads the danger circles bellow the objects
    osg::ref_ptr<osg::PositionAttitudeTransform> danger_group_trf =loadDangerCircles();

    perspectiveCam->addChild( danger_group_trf );

    // Loads the next position preview
    osg::ref_ptr<osg::PositionAttitudeTransform> next_pos_trf =loadNextPositionDisplay();
    perspectiveCam->addChild( next_pos_trf );

    // Loads the last position avatar
    osg::ref_ptr<osg::PositionAttitudeTransform> avatar_trf =loadDisplayForLastPosition(false);
    perspectiveCam->addChild( avatar_trf );


    perspectiveCam->addChild(markerTransf);
    perspectiveCam->addChild(shadowTransf);

    // Starts the viewer
    // create a root's viewer
    osgViewer::Viewer viewer;
    viewer.setUpViewInWindow(0,100,640,580);

    viewer.setSceneData(root.get());

    // Adds the controller to the event handler
    viewer.addEventHandler( ctrler.get() ); 

    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr kdtree (new pcl::search::KdTree<pcl::PointXYZRGBA>);   

    kdtree->setInputCloud (this->clean_cloud);

    std::cout << "Starting viewer ... " << std::endl;

    osg::ref_ptr<osg::PositionAttitudeTransform> tracePathTransf = new osg::PositionAttitudeTransform;
    perspectiveCam->addChild(tracePathTransf);

    
    
    osg::ref_ptr<osg::Camera> hud = createHUD(); //Initializes HUD
    root->addChild(hud);

    // Creates a PCL Visualizer 
	/*pcl::visualization::PCLVisualizer *viewerPCL = new pcl::visualization::PCLVisualizer ("3D Viewer");
	viewerPCL->setBackgroundColor (0, 0, 0);

	viewerPCL->addCoordinateSystem (1.0);
	viewerPCL->initCameraParameters ();

    // Loads the current cloud objects onto the PCL viewer
    std::vector<DetectionHelper::ClassifiedObject> currentCloudObjects = this->classifiedObjectsBuffer[this->classifiedObjectsBuffer.size()-1];
    for (int i = 0; i<currentCloudObjects.size(); i++){
        DetectionHelper::ClassifiedObject object = currentCloudObjects[i];
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr objCloud = object.cloud;
        DetectionHelper::ObjectClassification objClass = object.classification;
        
        if(objClass == DetectionHelper::ObjectClassification::WALL){
            viewerPCL->addPointCloud<pcl::PointXYZRGBA> (objCloud, "blue cloud" + std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "blue cloud"+ std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0,0,1,"blue cloud"+ std::to_string(i));
        }
        else if(objClass == DetectionHelper::ObjectClassification::OTHER){
            viewerPCL->addPointCloud<pcl::PointXYZRGBA> (objCloud, "green cloud" + std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "green cloud"+ std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0,1,0,"green cloud"+ std::to_string(i));
        }
        else if(objClass == DetectionHelper::ObjectClassification::ROBOT){
            viewerPCL->addPointCloud<pcl::PointXYZRGBA> (objCloud, "red cloud" + std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "red cloud"+ std::to_string(i));
            viewerPCL->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1,0,0,"red cloud"+ std::to_string(i));
            for(int i=0; i<object.childObjects.size(); i++){
                DetectionHelper::ClassifiedObject objectOnTop = object.childObjects[i];
                viewerPCL->addPointCloud<pcl::PointXYZRGBA> (objectOnTop.cloud, "white cloud" + std::to_string(i));
                viewerPCL->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "white cloud"+ std::to_string(i));
                viewerPCL->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1,1,1,"white cloud"+ std::to_string(i));
            }
        }
    }*/

    //Runs viewers
    ctrler->setModeChanged(true);
    ctrler->setMoved(true);
    while (!viewer.done() )
    {
        // Creates PCL visualizer to view objects
        //viewerPCL->spinOnce (100);
        
        // Checks if image mode has changed
        if(ctrler->acquireModeChanged()){
            unsigned char* colorData;
            switch (arImageMode)
            {
                case ARImageModes::RISK:
                    colorData = this->dataRiskColor;
                    break; 

                case ARImageModes::SHADING:
                    colorData = this->dataShadingFromRobot;
                    break;
                case ARImageModes::NORMAL:
                default:
                    colorData=this->data;
                    break;
            }
            image->setImage(cloud->width, cloud->height, 1 , GL_RGB, GL_RGB, GL_UNSIGNED_BYTE,colorData, osg::Image::NO_DELETE);
        }

        // Checks for movement of the marker        
        if(ctrler->acquireMovement()){
            std::cout << "Marker moved ... " << std::endl;
            // Resets previous tracePath;
            tracePathTransf->removeChildren(0,tracePathTransf->getNumChildren());
            float distance = 0; 
            bool addTracepath = traceMarkerPathToRobot(markerTransf,tracePathTransf, &distance);    
 
            // Stream to create text with info about distance
            std::stringstream ss;

            //Removes tracepath if it is not to draw
            if(!addTracepath){ 
                ss << "Distance to robot: n/a";
                tracePathTransf->removeChildren(0,tracePathTransf->getNumChildren());
            }
            else{ 
                ss << "Distance to robot: "<<distance << "cm";
            }
 
            //Updates the hud
            distanceTextHudFrame->setText(ss.str());

        }
        ctrler->acquireMovement(); //just a fancy way to set moved to false
        detectCollisions(markerTransf, kdtree, &collidedLeft, &collidedRight, &collidedFront, &collidedBack, &collidedBelow);
        viewer.frame();
    }
}



 //=============================================================================
//-------------------------AREA 51 (private methods)-----------------------------

/*  Conversion
    *  PCL to OSG
    *  X      -X div 100.0 
    *  Y      -Z div 100.0
    *  Z      -Y div 100.0 
*/

// Loads a display for last position. This is a bad name, because it will only do it
// When the last position is at a distance bigger than the robot width
osg::ref_ptr<osg::PositionAttitudeTransform> ARHelper::loadDisplayForLastPosition(bool showAlways){
    osg::ref_ptr<osg::PositionAttitudeTransform> transf = new osg::PositionAttitudeTransform;
    bool currentCloudHasRobot;
    std::vector<DetectionHelper::ClassifiedObject> robots = robotsBuffer(currentCloudHasRobot); // Gets all the robots

    // The display for last position is only valid when there is a last position to display
    if(currentCloudHasRobot && robots.size()>1){
        //Compute the distance between robots
        DetectionHelper::ClassifiedObject robot = robots[robots.size()-1];
        DetectionHelper::ClassifiedObject previousRobot =robots[robots.size()-2];
    
        osg::Vec3 distanceVector = pclToOsg(robot.centerOfMassAndMeanColor)- pclToOsg(previousRobot.centerOfMassAndMeanColor);
        float distance = distanceVector.length();

        // When the distance from last position is bigger than the robot width
        if(distance >= DetectionHelper::robotWidth*PCL_TO_OSG_SCALE_FACTOR || showAlways){
            // We will colorize the last position using the robot mean color
            double red,green,blue;
            // Lets unpack de colors and convert them to the space of 0-1
            red = ((robot.centerOfMassAndMeanColor.rgba >> 16)& 0x0000ff)/255.0;
            green = ((robot.centerOfMassAndMeanColor.rgba >> 8)& 0x0000ff)/255.0;
            blue = ((robot.centerOfMassAndMeanColor.rgba )& 0x0000ff)/255.0;

            //Creates the avatar geode
            osg::ref_ptr<osg::Geode> avatarGeode = createCylinder(osg::Vec3(0,0,0), DetectionHelper::robotWidth*PCL_TO_OSG_SCALE_FACTOR/2, DetectionHelper::robotHeight*PCL_TO_OSG_SCALE_FACTOR, red,green,blue,1.0);

            pcl::PointXYZRGBA centerPt = previousRobot.centerOfMassAndMeanColor;
            centerPt.y = -DetectionHelper::robotHeight/2; //Center of mass is for the whole robot including the volumetric marker and laser, which can have an impact 

            //transf->setScale(osg::Vec3(1,1,1));
            transf->setPosition(pclToOsg(centerPt));
            //transf->setAttitude(osg::Quat(0.0f, osg::Y_AXIS, 0.0f, osg::Z_AXIS, 0.0f, osg::X_AXIS));
            //transf->setDataVariance( osg::Object::DYNAMIC );

            transf->addChild(avatarGeode);
        }

    }
    

    return transf;
}

// Predicts the next position and loads a shape indicating it. The shape will extend from the robot center until the next predicted position
osg::ref_ptr<osg::PositionAttitudeTransform> ARHelper::loadNextPositionDisplay(){
    osg::ref_ptr<osg::PositionAttitudeTransform> capTransf = new osg::PositionAttitudeTransform;
    int robotIndex = getRobotIndex();
    if(robotIndex!=-1){
        // Gets the robot
        DetectionHelper::ClassifiedObject robot = classifiedObjectsBuffer[classifiedObjectsBuffer.size()-1][robotIndex];
        osg::Vec3 nextPosition, currentOrientation, currentPosition, speedVector;
        float yaw, speed;
        // Predicts the position
        bool canPredict = predictRobotNextStep(nextPosition, currentOrientation, currentPosition, speedVector, yaw, speed);
        if(canPredict){
            std::cout << "\n\nRobot speed " << speed << "; yaw " << yaw << "; orientation x " << currentOrientation.x() << ", y " << currentOrientation.y() << ", z " << currentOrientation.z() << std::endl;
            std::cout << "Robot current position " << "x " << currentPosition.x() << ", y " << currentPosition.y() << ", z " << currentPosition.z() << std::endl;
            std::cout << "Robot predicted next position " << "x " << nextPosition.x() << ", y " << nextPosition.y() << ", z " << nextPosition.z() << std::endl;

            // Computes capsule params
            double capRadius = PCL_TO_OSG_SCALE_FACTOR*robot.width/2;
            double capHeight = speed;
            osg::Vec3 capCenter = (nextPosition+currentPosition)/2; // cap center is given by the mean point between the predicted next, and current position
            capCenter = osg::Vec3(capCenter.x(), capCenter.y(), capCenter.z()*0.1); // Positions the capsule center close to the floor
            osg::ref_ptr<osg::Geode> capGeode = createCapsule(osg::Vec3(0,0,0), capRadius, capHeight, 0.5,0.5,0.5,1.0);
            
            // This will "crush the shape", like if it was a projection in XoY
            capTransf->setScale(osg::Vec3(.2,1,1)); 
            capTransf->setPosition(capCenter); //Sets to the computed position
            capTransf->addChild(capGeode.get()); // Adds the loaded geode to the transform
            osg::Quat capsAttitude( 0, osg::X_AXIS, -osg::PI/2., osg::Y_AXIS, yaw, osg::Z_AXIS ); //applies a roll (to "lie" the capsule on the floor), and a yaw, corresponding to the estimated robot orientation
            capTransf->setAttitude(capsAttitude);
        }      
    }

    return capTransf;
}


osg::ref_ptr<osg::PositionAttitudeTransform> ARHelper::loadArrowsOnObjects(){

    osg::ref_ptr<osg::Group> arrows_group = new osg::Group; //
    osg::ref_ptr<osg::PositionAttitudeTransform> arrows_group_tf = new osg::PositionAttitudeTransform;
    
    std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = getCurrentCloudObjects();

    for(int i=0; i<classifiedObjects.size(); i++){
        // Gets the detected object
        DetectionHelper::ClassifiedObject clsObj = classifiedObjects[i];
        // We will only place the arrows above "OTHER" objects
        if(clsObj.classification == DetectionHelper::ObjectClassification::OTHER){
            pcl::PointXYZRGBA objCenter = clsObj.centerOfMassAndMeanColor;
            pcl::PointXYZRGBA objHeighestPt = clsObj.minPt;//in the pcl coordinates the objects are upside down

            double objHeight = clsObj.height;
            double red,green,blue;
            // Lets unpack de colors and convert them to the space of 0-1
            red = ((objCenter.rgba >> 16)& 0x0000ff)/255.0;
            green = ((objCenter.rgba >> 8)& 0x0000ff)/255.0;
            blue = ((objCenter.rgba )& 0x0000ff)/255.0;
            std::cout << " rgba " << objCenter.rgba<< " red " << objCenter.r << " red unpacked " << red;// << std::endl;
            std::cout << " green " << objCenter.g << " green unpacked " << green;
            std::cout << " blue " << objCenter.b << " blue unpacked " << blue << std::endl;

            // Builds the material for the arrow with the mean color of the object it is pointing to
            osg::Material *material = new osg::Material();
            material->setDiffuse(osg::Material::FRONT,  osg::Vec4(red, green, blue, 1.0));
            material->setSpecular(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
            material->setAmbient(osg::Material::FRONT,  osg::Vec4(0.1, 0.1, 0.1, 1.0));
            material->setEmission(osg::Material::FRONT, osg::Vec4(0.3, 0.3, 0.3, 1.0));
            material->setShininess(osg::Material::FRONT, 100.0);

            //Transforms for the arrow
            osg::ref_ptr<osg::PositionAttitudeTransform> arrowTransf = new osg::PositionAttitudeTransform;
            osg::ref_ptr<osg::Node> arrowNode = loadVirtualObject(material,ARROW_MODEL_PATH);

            //std::cout << arrowNode->height << std::endl;
            // Sets the arrow transform params
            arrowTransf->setDataVariance( osg::Object::DYNAMIC );
            // Sets position. Adds an offset of 10 to height
            //arrowTransf->setPosition(osg::Vec3(-objCenter.x*100.0,-objCenter.z*100.0,10+(-objCenter.y+ objHeight/2)*100.0)); //use the min point from classified obj instead of center
            
            //osg::Vec3 arrowPos = osg::Vec3(-objCenter.x*100.0, -objCenter.z*100.0, 10+(-objHeighestPt.y)*100.0);
            osg::Vec3 arrowPos = osg::Vec3(-objCenter.x*100.0, -objCenter.z*100.0, 10+(-objHeighestPt.y)*100.0);
            arrowTransf->setPosition(arrowPos); //use the min point from classified obj instead of center
            
            std::cout << " arrow pos x " << arrowPos.x() << " y "<< arrowPos.y() << " z " << arrowPos.z() << std::endl;


            arrowTransf->setAttitude(osg::Quat(0.0f, osg::Y_AXIS, 0.0f, osg::Z_AXIS, M_PI, osg::X_AXIS));
            arrowTransf->addChild(arrowNode.get());

            arrows_group->addChild(arrowTransf);
        }
    }

    // Builds the animation call back. Since we points will be all pointing in the same direction
    // We just set the same animation for all.
    osg::ref_ptr<osg::AnimationPathCallback> apcb = new osg::AnimationPathCallback;
    apcb->setAnimationPath( createLoopJumpAnimationPath(2.0, 5));
    arrows_group_tf->setUpdateCallback( apcb.get() );

    arrows_group_tf->addChild(arrows_group);
    return arrows_group_tf;
}



void ARHelper::runViewer(osg::ref_ptr<osg::Group> rootNode){
    // create a root's viewer
    osgViewer::Viewer viewer;
    viewer.setUpViewInWindow(0,0,640,480);
    viewer.setSceneData(rootNode.get());


    // Loop that generates constant images. Press ESC to close
    while (!viewer.done() )
    {
        viewer.frame();
    }
}


// Gets color and depth arrays from cloud
void ARHelper::extractPointCloudColorDepthData(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, unsigned char* data, unsigned char* dataDepth)
{
    // Iterates every point of the cloud and gets its color and depth info
    long index1, index2;
    for (int row=0;row<cloud->height;row++)
    {
        for (int col=0;col<cloud->width;col++)
        {
            index1 = 3*(row*cloud->width + col); // index in the data arrays
            index2 = (cloud->height-row-1)*cloud->width + col; //

            // Gets the rgb channels of this point into the data vector
            data[index1] = cloud->points[index2].r;
            data[index1+1] = cloud->points[index2].g;
            data[index1+2] = cloud->points[index2].b;

            // We will for sure need this verification here, because the original cloud is not guarantee do be dense
            if (isnan(cloud->points[index2].x)){ 
                dataDepth[index1] = dataDepth[index1+1] = dataDepth[index1+2] = 0;
            }else{ // Open GL requires depth data to be in a scale of [0,255]
                dataDepth[index1] = ((cloud->points[index2].x+2)/6.0)*255.0;
                dataDepth[index1+1] = ((cloud->points[index2].y+2)/6.0)*255.0;
                dataDepth[index1+2] = (cloud->points[index2].z/10.0)*255.0;
            }

        }
    }
}
// Extracts a color array from the point cloud, expressing the risk of collision between robot and rest of the cloud
// Some areas will be painted as black because the cloud is not guarantee to be dense
void ARHelper::extractPointCloudColorCollisionRiskData(unsigned char* data){
    int robotIndex = getRobotIndex();

    if(robotIndex > 0){
        
        // We need to rotate the cloud point in each iteration, since the original cloud is not aligned with the camera
        Eigen::Affine3f t1 = pcl::getTransformation(0.0, -this->height, 0.0, 0.0,0.0, 0);
        Eigen::Affine3f r1 = pcl::getTransformation (0.0, 0.0, 0.0, -this->pitch, 0.0, 0.0);
        Eigen::Affine3f r2 = pcl::getTransformation (0.0, 0.0, 0.0, 0.0, 0.0, -this->roll);
        Eigen::Affine3f ptT = t1*r1*r2;
        //pcl::transformPoint()

        DetectionHelper::ClassifiedObject robot = this->classifiedObjectsBuffer[this->classifiedObjectsBuffer.size()-1][robotIndex];
        pcl::PointXYZRGBA robotCenter = robot.centerOfMassAndMeanColor;
        pcl::PointXYZRGBA robotMin = robot.minPt;
        pcl::PointXYZRGBA robotMax = robot.minPt;
        double robotRadius = DetectionHelper::robotWidth/2 ; //robot.width/2;
        pcl::PointXYZRGBA farthestPoint = DetectionHelper::findMostDistantPoint(cloud, robotCenter);
        

        // Projects the points in XoZ, because the robot only moves in XoZ
        pcl::PointXYZRGBA farthestPointXoZ = farthestPoint;
        farthestPointXoZ.y = 0;
        pcl::PointXYZRGBA robotCenterXoZ = robotCenter;
        robotCenterXoZ.y = 0;
        double maxDistance = DetectionHelper::distanceBetweenPoints(robotCenter, farthestPoint);
        double maxDistanceXoZ = DetectionHelper::distanceBetweenPoints(robotCenterXoZ, farthestPointXoZ);

        // The risk is calculated based on the distance from point to robot
        long index1, index2;
        for (int row=0;row<cloud->height;row++)
        {
            for (int col=0;col<cloud->width;col++){
                index1 = 3*(row*cloud->width + col); // index in the data array
                index2 = (cloud->height-row-1)*cloud->width + col; // index in the cloud points array
                // gets point from cloud and transforms it to be aligned with the camera referential
                pcl::PointXYZRGBA cloudPoint;
                if(isnan(cloud->points[index2].x))
                {
                    cloudPoint.x=cloudPoint.y=cloudPoint.z=0;
                }else{
                    cloudPoint = pcl::transformPoint( cloud->points[index2], ptT);
                }
        

                // Gets the projected version of the point
                pcl::PointXYZRGBA cloudPointXoZ = cloudPoint;
                cloudPointXoZ.y=0;

                //Computes distance to robot
                double distance = DetectionHelper::distanceBetweenPoints(robotCenter, cloudPoint);
                double distanceXoZ = DetectionHelper::distanceBetweenPoints(robotCenterXoZ, cloudPointXoZ);
                double riskFactor = (distance <= robotRadius) ? 0:distanceXoZ/(maxDistanceXoZ);
                
                // The riskFactor is linear in scale from 0 to 1
                // now we induce a non linearity. 200^(-x)
                //riskFactor = 1 - pow(50.0f, -riskFactor);
                //riskFactor = 1 - (- 1.05*pow(20.0f, riskFactor-1) + 1.05);
                //riskFactor = 1 - (- 1.05*pow(20.0f, riskFactor-1) + 1.05);
                //riskFactor = 1 - (2*cos(riskFactor)-1);
                // 1.029\cdot\cos\left(x\cdot1.5\ -\ p\right)+1.029
                //riskFactor = 1- (1.029*cos(1.5*riskFactor+1.6)+1.029);

                double a= -6, b=16.5;
                riskFactor = 1-(1/(1+exp(a+b*riskFactor))); // this is 1-sigmoid


                uint8_t riskColor = 255*riskFactor;
                uint8_t grayScaleColor = (0.3*cloud->points[index2].r + 0.59*cloud->points[index2].g + 0.11*cloud->points[index2].b);

                uint8_t finalColor = 255 - ((255*riskFactor ));//* 0.8) + (0.3*cloud->points[index2].r + 0.59*cloud->points[index2].g + 0.11*cloud->points[index2].b) * 0.2);     
                
                if(cloudPoint.y >= -DetectionHelper::robotHeight && cloudPoint.y <= -0.02){// && cloudPoint.y <= 0){
                    data[index1] = finalColor;
                    data[index1+1] = robotRadius <= distanceXoZ && distanceXoZ <= robotRadius+0.2? 0: finalColor;
                    data[index1+2] =  robotRadius <= distanceXoZ && distanceXoZ <= robotRadius+0.2? 0:finalColor;
                }
                else{
                    grayScaleColor = (grayScaleColor+255)/2;
                    data[index1] = grayScaleColor;
                    data[index1+1] = grayScaleColor;
                    data[index1+2] =  grayScaleColor;    
                }


            }
        }
    }
}

// Extracts a color array from the point cloud, expressing the risk of collision between robot and rest of the cloud
void ARHelper::extractPointCloudColorShadingFromRobot(unsigned char* data){

}

// Creates an ortho texture to be used in orthoCam
void ARHelper::createOrthoTexture(osg::ref_ptr<osg::Geode> orthoTextureGeode, osg::Image *image, unsigned char* data, unsigned char* dataDepth, int width, int height)
{
    // Creates two images, one with color, other with depth data
    // That will be drawn from the top left of the viewport
    osg::Image *imageDepth = new osg::Image;
    image->setOrigin(osg::Image::TOP_LEFT);
    imageDepth->setOrigin(osg::Image::TOP_LEFT);
    image->setImage(width, height, 1 , GL_RGB, GL_RGB, GL_UNSIGNED_BYTE,data, osg::Image::NO_DELETE);
    imageDepth->setImage(width, height, 1 , GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, dataDepth, osg::Image::NO_DELETE);

    // Then uses the images to create textures
    osg::ref_ptr<osg::Texture2D> colorTexture = new osg::Texture2D;
    osg::ref_ptr<osg::Texture2D> depthTexture = new osg::Texture2D;
    colorTexture->setImage( image );
    depthTexture->setImage(imageDepth);

    // Creates a 2D quadrilateral shape with heigh and width 1, and associates color and depth textures to it
    osg::ref_ptr<osg::Drawable> quad = osg::createTexturedQuadGeometry( osg::Vec3(), osg::Vec3(1.0f, 0.0f, 0.0f), osg::Vec3(0.0f, 1.0f, 0.0f) );
    quad->getOrCreateStateSet()->setTextureAttributeAndModes(0, colorTexture.get());
    quad->getOrCreateStateSet()->setTextureAttributeAndModes(1, depthTexture.get());

    // Creates shaders to process vertexes and fragments (resulting from the projection of the quad in the screen)
    osg::ref_ptr<osg::Shader> vertShader = new osg::Shader( osg::Shader::VERTEX, ARHelper::textureVertexSource);
    osg::ref_ptr<osg::Shader> fragShader =  new osg::Shader( osg::Shader::FRAGMENT, ARHelper::textureFramentSource);   
    osg::ref_ptr<osg::Program> program = new osg::Program;
    program->addShader( fragShader.get() );
    program->addShader( vertShader.get() );

    // This shaders will associate the depth data to the pixels of the quadrilateral
    // This will make the GPU consider that this quadrilateral pixels have a variable depth, which will be useful to 
    // manage occlusions between real and virtual elements
    quad->setDataVariance(osg::Object::DYNAMIC);
    quad->getOrCreateStateSet()->setAttributeAndModes( program.get() );
    quad->getOrCreateStateSet()->addUniform(new osg::Uniform("texture", 0 ));
    quad->getOrCreateStateSet()->addUniform(new osg::Uniform("textureDepth", 1 ));

    orthoTextureGeode->addDrawable( quad.get() );
    
}


// TODO: REESCREVER OS COMENTÁRIOS E MUDAR UM POUCO O CÓDIGO PARA NÃO SER SÓ COPY PASTE DO LAB 5
// Defines virtual ortho and perspective cameras, aplying the pitch, roll and height transforms 
void ARHelper::setVirtualCameras(osg::ref_ptr<osg::Camera> orthoCamera, osg::ref_ptr<osg::Camera> perspectiveCamera, osg::ref_ptr<osg::Geode> orthoTexture)
{
    
    //25. Cria a cam responsável por olhar para o quadrilátero com a imagem produzida pelo sensor. 
    //    É configurada para obter projecções ortográficas
    //    É importante notar que é configurada de forma a ser a primeira a produzir imagem. Ver POST_RENDER_ORDER
    orthoCamera->setCullingActive( false );
    orthoCamera->setClearMask( 0 );
    orthoCamera->setAllowEventFocus( false );
    orthoCamera->setReferenceFrame( osg::Camera::ABSOLUTE_RF );
    orthoCamera->setRenderOrder( osg::Camera::POST_RENDER, 0);
    orthoCamera->setProjectionMatrix( osg::Matrix::ortho(0.0, 1.0, 0.0, 1.0, 0.5 , 1000) );
    orthoCamera->addChild( orthoTexture.get() );
    osg::StateSet* ss = orthoCamera->getOrCreateStateSet();
    ss->setMode( GL_LIGHTING, osg::StateAttribute::OFF );


    //26. Cria a segunda cam (perspectiva). As imagens desta serão obtidas após a primeira, de forma a que o seu output se sobreponha ao da cam ortografica
    //    Esta câmera vai apenas visualizar elementos 3D que se encontrem a uma distancia entre 0.5 e 1000. 
    //    As configs de setProjectionMatrixAsPerspective, garantem que a cam tem as mesmas configs que o sensor real
    perspectiveCamera->setCullingActive( false );
    perspectiveCamera->setClearMask( 0 );
    perspectiveCamera->setAllowEventFocus( false );
    perspectiveCamera->setReferenceFrame( osg::Camera::ABSOLUTE_RF );
    perspectiveCamera->setRenderOrder( osg::Camera::POST_RENDER, 1);
    perspectiveCamera->setProjectionMatrixAsPerspective(50, 640.0/480.0, 0.5, 1000);

    // Vamos agora colocar uma camara virtual, de acordo com a pose estimada anteriormente

    //27. O seguinte codigo permite criar uma primeira matriz de rotaçao que terá em conta o angulo de pitch da câmara
    osg::Matrixd cameraRotation2;
    cameraRotation2.makeRotate(osg::DegreesToRadians(pitch*180/M_PI), osg::Vec3(1,0,0),
                                                        osg::DegreesToRadians(0.0), osg::Vec3(0,1,0),                                 
                                                        osg::DegreesToRadians(0.0), osg::Vec3(0,0,1));    


    //28. Esta terá em conta o roll
    osg::Matrixd cameraRotation3;
    cameraRotation3.makeRotate(osg::DegreesToRadians(0.0), osg::Vec3(1,0,0),
    osg::DegreesToRadians(roll*180/M_PI), osg::Vec3(0,1,0),
    osg::DegreesToRadians(0.0), osg::Vec3(0,0,1));  

    //29. A matriz seguinte implementa uma transçaão que tem em conta a altura da cam em relação ao plano do chão. O facto 100 visa compensa diferença
    // de escalas entre OSG e PCL
    osg::Matrixd cameraTrans;
    cameraTrans.makeTranslate(0, 0, height*100.0);

    //30. As seguintes matrizes servem para garntiar que as diferenças de referencial entre OSG e PCL são tidas em conta:
    osg::Matrix matrix_view;
    matrix_view.makeLookAt(osg::Vec3(0, 0, 0), osg::Vec3(0, -1, 0), osg::Vec3(0, 0, 1));
    osg::Matrixd cameraRotation;
    cameraRotation.makeRotate(osg::DegreesToRadians(180.0), osg::Vec3(0,0,1), osg::DegreesToRadians(-90.0), osg::Vec3(1,0,0), osg::DegreesToRadians(0.0), osg::Vec3(0,1,0) );

    //31. Vamos multiplicar todas as matrizes produzindo uma mat final que é fornecida ao OSG para que este saiba onde colocar a camara virtual
    // a inversa surge por requisitos internos do OSG
    perspectiveCamera->setViewMatrix(osg::Matrix::inverse(cameraRotation*cameraRotation3*cameraRotation2*cameraTrans)); 

    //32. Define o tamanho do viewport de cada uma das câmaras, ou seja, tamanho e posição das imagens geradas
    // esta info serve para saber onde vamos colocar imagens no ecrã. 
    orthoCamera->setViewport(0, 0, 640, 480);
    perspectiveCamera->setViewport(0, 0, 640, 480);   

}


// From RMA information page. Creates a phongShader to be used to shade virtual objects
osg::ref_ptr<osg::Program> ARHelper::createPhongShaderProgram(){

    osg::ref_ptr<osg::Shader> phongVertShader = new osg::Shader( osg::Shader::VERTEX, phongVertexSource );
    osg::ref_ptr<osg::Shader> phongFragShader = new osg::Shader( osg::Shader::FRAGMENT, phongFragmentSource );
    osg::ref_ptr<osg::Program> phongShaderProgram = new osg::Program;
    phongShaderProgram->addShader( phongVertShader.get() );
    phongShaderProgram->addShader( phongFragShader.get() );
    return phongShaderProgram;
}

// Loads a virtual object into node, with a default material
osg::ref_ptr<osg::Node>  ARHelper::loadVirtualObject(std::string objPath){
    osg::Material *material = new osg::Material();
    material->setDiffuse(osg::Material::FRONT,  osg::Vec4(1.0, 0.5, 0.0, 1.0));
    material->setSpecular(osg::Material::FRONT, osg::Vec4(1.0, 0.5, 0.0, 1.0));
    material->setAmbient(osg::Material::FRONT,  osg::Vec4(0.1, 0.1, 0.1, 1.0));
    material->setEmission(osg::Material::FRONT, osg::Vec4(0.0, 0.0, 0.0, 1.0));
    material->setShininess(osg::Material::FRONT, 100.0);

    return loadVirtualObject(material, objPath);
}

osg::ref_ptr<osg::Node> ARHelper::loadVirtualObject( osg::Material *material, std::string objPath){
    osg::ref_ptr<osg::Node> node = osgDB::readNodeFile(objPath);
    node->getOrCreateStateSet()->setMode( GL_LIGHT1, osg::StateAttribute::ON );
    node->getOrCreateStateSet()->setAttribute(material);
    node->getOrCreateStateSet()->setAttributeAndModes( createPhongShaderProgram().get() );
    return node;
}


// TODO: A animação está um pouco "feia". Não possui um loop suave. fica para backlog
osg::AnimationPath* ARHelper::createLoopJumpAnimationPath( float time, float maxHeight){
    // animation path object
    osg::ref_ptr<osg::AnimationPath> path = new osg::AnimationPath;
    // Sets the animation to be performed in loops
    path->setLoopMode( osg::AnimationPath::LOOP );
    // the number of key poses that we will define; the higher, the smoother the circular trajectory
    unsigned int numSamples = 32;

    // Height increment on each path key pose
    float delta_height = maxHeight/((float)numSamples-1.0f);
    // Time the object spends on each height increment
    float delta_time = time / (float)numSamples;
    

    // A pre increment is made because in a loop, we don't want the object to spend extra time in the first state
    // Remember APC subject on animating keyframes in blender!
    int midIteration = numSamples/2;
    for(unsigned int i=0; i<numSamples; ++i){
        float heightIncrement;

        if(i >= midIteration ){ // down
            heightIncrement = delta_height * (float)(numSamples - i); 
        }
        else{ //up
            heightIncrement = delta_height * (float)i; 
        }

        // compute the projection of the vector
        osg::Vec3 pos(0,0,heightIncrement);
        //osg::Vec3 pos(0,-150,30+heightIncrement); //heightIncrement
        //Add keypose
        path->insert(delta_time*(float)i, osg::AnimationPath::ControlPoint(pos));
    }

    return path.release();
}

//Gets robot index in the current cloud
int ARHelper::getRobotIndex(){
    return getRobotIndex(classifiedObjectsBuffer.size()-1);
}

//Gets robot index in a specific cloud
int ARHelper::getRobotIndex(int bufferIdx){
    std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = classifiedObjectsBuffer[bufferIdx];
    //TODO: replace this with an iterator in the future
    for(int i=0; i<classifiedObjects.size(); i++){
        DetectionHelper::ClassifiedObject clsObj = classifiedObjects[i];
        if(clsObj.classification==DetectionHelper::ObjectClassification::ROBOT)
            return i;
    }
    return -1;
}


// Loads danger circles bellow each "OTHER" object in the form of cilinders
osg::ref_ptr<osg::PositionAttitudeTransform> ARHelper::loadDangerCircles(){
    
    osg::ref_ptr<osg::Group> cyl_group = new osg::Group; 
    osg::ref_ptr<osg::PositionAttitudeTransform> cyl_group_tf = new osg::PositionAttitudeTransform;

    // Gets the current cloud robot index
    int robotIndex = getRobotIndex();

    // Gets the current cloud classified objects 
    std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = getCurrentCloudObjects();

    //For each classified object of type "OTHER", loads a cylinder bellow it
    //If there is a robot in town, the cylinder will be more red or green acording if it is too close of too distance
    //If there is no robot in town, the cylinder is painted blue 
    for(int i=0; i<classifiedObjects.size(); i++){
        DetectionHelper::ClassifiedObject clsObj = classifiedObjects[i];
        if(clsObj.classification == DetectionHelper::ObjectClassification::OTHER){
            //Note: the objects extracted using PCL are pointing down, so we use their maxPt as their lowest point. 

            // Gets the danger circle position for this object
            pcl::PointXYZRGBA objLowestPoint = clsObj.maxPt;
            pcl::PointXYZRGBA objCenterPt = clsObj.centerOfMassAndMeanColor;
            pcl::PointXYZRGBA pclCylPos = objCenterPt;
            pclCylPos.y = objLowestPoint.y;
            osg::Vec3 osgCylPos = pclToOsg(pclCylPos);

            //Will contain the center of mass for the danger circle, preferably leveled to the robot level
            pcl::PointXYZRGBA objLeveledCenterPt = objCenterPt;
            objLeveledCenterPt.y = -DetectionHelper::robotHeight/4; 
            // Lets decide the color for the cylinder. First we check for robot
            double r=0.0,g=0.0,b=0.0,a=1.0;
            if(robotIndex == -1){ // No robot
                b=1.0; // we set to blue the cylinder color

                //Since there is no robot, we compute the value for an "hipotetical" robot
                
            }
            else{ // There is a robot
                DetectionHelper::ClassifiedObject robotObj = classifiedObjects[robotIndex];
                // So now we compute the distance between robot and object
                // but to compute this more precisely, we need to "lower" the object center
                // of mass to the level of the robot 
                pcl::PointXYZRGBA robotCenter = robotObj.centerOfMassAndMeanColor;
                robotCenter.y = objLeveledCenterPt.y;
                double distance = DetectionHelper::distanceBetweenPoints(objLeveledCenterPt, robotCenter);
                
                // We are assuming the optimal distance is half of the robot width
                // The proportion is given on the ratio betwen distance and robot width
                // We also clamp it to a max of 1
                // 0 will correspond to red, and 1 to green
                double ratio = std::min(distance , 1.0); 

                std::cout << "Robot distance to object " << i << " is " << distance << " and ratio " << ratio << std::endl;
                g=ratio;
                r=1-ratio;                
            }

            double dangerCylRadius = ((clsObj.width+clsObj.width*.05)/2)*PCL_TO_OSG_SCALE_FACTOR;
            double dangerCylHeight = (DetectionHelper::robotHeight) *PCL_TO_OSG_SCALE_FACTOR;

            osg::ref_ptr<osg::PositionAttitudeTransform> cylTransf = new osg::PositionAttitudeTransform;
            cylTransf->setScale(osg::Vec3(1,1,1));
            cylTransf->setPosition(pclToOsg(objLeveledCenterPt));
            cylTransf->setAttitude(osg::Quat(0.0f, osg::Y_AXIS, 0.0f, osg::Z_AXIS, 0.0f, osg::X_AXIS));
            cylTransf->setDataVariance( osg::Object::DYNAMIC );

            osg::ref_ptr<osg::Geode> cylGeode = createCylinder(osg::Vec3(0,0,0), dangerCylRadius, dangerCylHeight, r,g,b,a);
            
            cylTransf->addChild(cylGeode.get());
            cyl_group->addChild(cylTransf);
        }
    }   

    cyl_group_tf->addChild(cyl_group);
    return cyl_group_tf;
}


// Creates a cylinder geode
// Based on http://www.sm.luth.se/csee/courses/smm/011/l/t1.pdf
osg::ref_ptr<osg::Geode> ARHelper::createCylinder(osg::Vec3 center, double cylRadius, double cylHeight, double r, double g, double b, double a){
    //Creates the cylinder material
    /*osg::Material *material = new osg::Material();
    material->setDiffuse(osg::Material::FRONT,  osg::Vec4(r, g, b, a));
    material->setSpecular(osg::Material::FRONT, osg::Vec4(1.0, 0.5, 0.0, 1.0));
    material->setAmbient(osg::Material::FRONT,  osg::Vec4(0.1, 0.1, 0.1, 1.0));
    material->setEmission(osg::Material::FRONT, osg::Vec4(0.0, 0.0, 0.0, 1.0));
    material->setShininess(osg::Material::FRONT, 100.0);*/
    
    // Creates the geode that encapsulates the shape
    osg::ref_ptr<osg::Geode> cylGeo = new osg::Geode;
    // Creates the cylinder describer
    osg::ref_ptr<osg::Cylinder> cyl = new osg::Cylinder(center, cylRadius, cylHeight);
    // Creates the drawable
    osg::ref_ptr<osg::ShapeDrawable> cylShape = new osg::ShapeDrawable(cyl.get());
    cylShape->setColor(osg::Vec4(r,g,b,a));
    cylGeo->addDrawable(cylShape.get());

    cylGeo->getOrCreateStateSet()->setMode( GL_LIGHT1, osg::StateAttribute::ON ); // TODO ver o que é o GL_LIGHT1
    //cylGeo->getOrCreateStateSet()->setAttribute(material);
    cylGeo->getOrCreateStateSet()->setAttributeAndModes( createPhongShaderProgram().get() );

    return cylGeo;
}

// Goes through previous cloud analysis, and gets the average robot speed, based on the distance between each step
// In realtime, this would turn into an exponentially expensive task, so in that case would be more responsible to consider only
// the last K clouds. Since this is an academic experience with limited clouds, we will iterate over all clouds analysed 
// We will only consider for mean speed the clouds where a robot is detected
float ARHelper::averageRobotSpeed(){
    int numCloudsWithRobot = 0; 
    // Iterates over all the clouds analysis
    DetectionHelper::ClassifiedObject previousRobot; // Saves robot info on a previous cloud
    float distance = 0;
    
    //Sums the distances between current robot position and the last, and returns it's mean
    for(int i=0; i<classifiedObjectsBuffer.size(); i++){
        std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = classifiedObjectsBuffer[i]; //Gets the classified objects of a specific analysis
        int robotIndex = getRobotIndex(i);
        if(robotIndex!=-1){ 
            DetectionHelper::ClassifiedObject robot = classifiedObjects[robotIndex];
            if(numCloudsWithRobot > 0){ //The first time robot appears we won't consider it for speed calculation
                pcl::PointXYZRGBA robotCenter = robot.centerOfMassAndMeanColor;
                pcl::PointXYZRGBA previousRobotCenter = previousRobot.centerOfMassAndMeanColor;
                robotCenter.y=0; previousRobotCenter.y=0;
                osg::Vec3 osgRbCenter = pclToOsg(robotCenter);
                osg::Vec3 osgPrevRbCenter = pclToOsg(previousRobotCenter);

                distance+=(osgRbCenter-osgPrevRbCenter).length();
                //distance +=DetectionHelper::distanceBetweenPoints(robotCenter, previousRobotCenter);
            }
            previousRobot = robot;
            numCloudsWithRobot++;
        }
    }
    return numCloudsWithRobot == 0? 0 : (distance/numCloudsWithRobot);
}

// Gets, for the current analysis, the robot orientation
// Returns false in case o insucess
// orientation.y will be always 0
// The vector will be returned normalized
bool ARHelper::getRobotOrientation(osg::Vec3 &orientation){
    bool success = false;
    // The robot orientation is given based on what is on top of if
    int robotIndex = getRobotIndex();
    DetectionHelper::ClassifiedObject robot = classifiedObjectsBuffer[classifiedObjectsBuffer.size()-1][robotIndex];

    //Gets the marker and laser indexes
    int markerIndex=-1, laserIndex=-1;
    for(int i=0; i<robot.childObjects.size(); i++){
        if(robot.childObjects[i].classification==DetectionHelper::ObjectClassification::LASER)
            laserIndex=i;
        if(robot.childObjects[i].classification==DetectionHelper::ObjectClassification::VOLUMETRIC_MARKER)
            markerIndex=i;   
    }

    pcl::PointXYZRGBA pclOrientation, frontPoint, backPoint;

    // Validates the indexes
    if(markerIndex+laserIndex >= -1){ //at least one of them must be defined
        if(laserIndex == -1){
            frontPoint = robot.centerOfMassAndMeanColor;
            std::cout << "\n\nUsing center of mass as frontPoint " << " x " << frontPoint.x << ", y " << frontPoint.y << ", z " << frontPoint.z << std::endl;
        }
        else{
            frontPoint = robot.childObjects[laserIndex].centerOfMassAndMeanColor;
            std::cout << "\n\nUsing LASER as frontPoint " << " x " << frontPoint.x << ", y " << frontPoint.y << ", z " << frontPoint.z << std::endl;
        }
        
        if(markerIndex == -1){
            backPoint = robot.centerOfMassAndMeanColor;
            std::cout << "\n\nUsing center of mass as backPoint " << " x " << backPoint.x << ", y " << backPoint.y << ", z " << backPoint.z << std::endl;
        }
        else{
            backPoint = robot.childObjects[markerIndex].centerOfMassAndMeanColor;
            std::cout << "\n\nUsing  MARKER as backPoint " << " x " << backPoint.x << ", y " << backPoint.y << ", z " << backPoint.z << std::endl;
        }
        
        //Determines the orientation in PCL coordinates
        pclOrientation.x=frontPoint.x-backPoint.x;
        pclOrientation.y=0; //Set to 0 due to the height differences between elements
        pclOrientation.z=frontPoint.z-backPoint.z;

        std::cout << "\n\nRobot orientation (PCL) " << " x " << pclOrientation.x << ", y " << pclOrientation.y << ", z " << pclOrientation.z << std::endl;
        // and converts it to osg
        orientation = pclToOsg(pclOrientation);
        std::cout << "\n\nRobot orientation (OSG) " << " x " << orientation.x() << ", y " << orientation.y() << ", z " << orientation.z() << std::endl;

        // normalize the orientation vector
        //double norm = sqrt(orientation.x()*orientation.x() +orientation.y()*orientation.y() + orientation.z()*orientation.z());
        orientation.normalize();// normalize is an inline function, and returns the previous length of the vector, which we don't need

        std::cout << "\n\nRobot orientation (OSG) normalized " << " x " << orientation.x() << ", y " << orientation.y() << ", z " << orientation.z() << std::endl;

        success = true;
    }

    return success;
}

// Predicts the next step of the robot based on its average speed and orientation, and returns its current orientation, position and speed
// z of speedVector and currentOrientation will be 0
// Returns false in case o insucess
bool ARHelper::predictRobotNextStep(osg::Vec3 &nextPosition, osg::Vec3 &currentOrientation, osg::Vec3 &currentPosition, osg::Vec3 &speedVector, float &yaw, float &speed){
    bool success = getRobotOrientation(currentOrientation);

    // If a orientation vector was computer with success
    if(success){
        // First we determine the speed vector
        speed = averageRobotSpeed();
        // Compute the speed vector being speed*orientation
        speedVector = currentOrientation*speed;

        int robotIndex = getRobotIndex();
        DetectionHelper::ClassifiedObject robot = classifiedObjectsBuffer[classifiedObjectsBuffer.size()-1][robotIndex];
        pcl::PointXYZRGBA robotCenter = robot.centerOfMassAndMeanColor;
        // Computer next position as being the sum of the robot center with the speed vector
        currentPosition = pclToOsg(robotCenter);
        nextPosition = currentPosition + speedVector;

        //yaw = -asin(currentOrientation.y()/currentOrientation.x());
        //yaw = isnan(yaw) ? 0 : yaw;

        yaw = atan2(currentOrientation.y(),currentOrientation.x());
    }

    return success;
}

osg::ref_ptr<osg::Geode> ARHelper::createCapsule(osg::Vec3 center, double radius, double height, double r, double g, double b, double a){
    // Creates the geode that encapsulates the shape
    osg::ref_ptr<osg::Geode> capGeo = new osg::Geode;

    osg::ref_ptr<osg::Capsule> capsule = new osg::Capsule(center, radius, height);
    
    // Creates the drawable
    osg::ref_ptr<osg::ShapeDrawable> capShape = new osg::ShapeDrawable(capsule.get());
    capShape->setColor(osg::Vec4(r,g,b,a));
    capGeo->addDrawable(capShape.get());

    capGeo->getOrCreateStateSet()->setMode( GL_LIGHT1, osg::StateAttribute::ON ); // TODO ver o que é o GL_LIGHT1
    //cylGeo->getOrCreateStateSet()->setAttribute(material);
    capGeo->getOrCreateStateSet()->setAttributeAndModes( createPhongShaderProgram().get() );
    
    return capGeo;

}



void ARHelper::CreateMarker(osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf,
                   osg::ref_ptr<osg::PositionAttitudeTransform> shadowTransf){


    osg::ref_ptr<osg::Shader> vertShader2 =
    new osg::Shader( osg::Shader::VERTEX, markerVertexSource );
    osg::ref_ptr<osg::Shader> fragShader2 =
    new osg::Shader( osg::Shader::FRAGMENT, markerFragmentSource );
    osg::ref_ptr<osg::Program> program2 = new osg::Program;
    program2->addShader( fragShader2.get() );
    program2->addShader( vertShader2.get() );

    osg::Texture2D *markerTexture = new osg::Texture2D;
    markerTexture->setDataVariance(osg::Object::DYNAMIC);
    markerTexture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
    markerTexture->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
    markerTexture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
    markerTexture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
    osg::Image *markerImage = osgDB::readImageFile("../Resources/10539_tennis_ball_diffuse_v1.jpg");
    markerTexture->setImage(markerImage);

    osg::ref_ptr<osg::Node> markerNode = osgDB::readNodeFile("../Resources/10539_tennis_ball_L3.obj");
    markerNode->getOrCreateStateSet()->setMode( GL_LIGHT1, osg::StateAttribute::ON );
    osg::StateSet *markerStateSet = markerNode->getOrCreateStateSet();
    markerStateSet->setTextureAttributeAndModes(0, markerTexture, osg::StateAttribute::ON);
    markerStateSet->setAttributeAndModes( program2.get() );
    markerStateSet->addUniform(new osg::Uniform("texture", 0 ));

    osg::Image *shadowImage = osgDB::readImageFile("../Resources/shadow.png");
    osg::ref_ptr<osg::Texture2D> shadowTexture = new osg::Texture2D(shadowImage);
    osg::ref_ptr<osg::StateSet> shadowStateSet = new osg::StateSet();
    shadowStateSet->setTextureAttributeAndModes(0, shadowTexture, osg::StateAttribute::ON);
    shadowStateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    osg::ref_ptr<osg::BlendFunc> texture_blending_function = new osg::BlendFunc();
    shadowStateSet->setAttributeAndModes(texture_blending_function.get(), osg::StateAttribute::ON);
    shadowStateSet->setAttributeAndModes( program2.get() );
    shadowStateSet->addUniform(new osg::Uniform("texture", 0 ));

    osg::ref_ptr<osg::Drawable> shadowQuad =
    osg::createTexturedQuadGeometry( osg::Vec3(0.5f, 0.5f, 0.0f), osg::Vec3(-1.0f, 0.0f, 0.0f), osg::Vec3(0.0f, -1.0f, 0.0f) );
    shadowQuad->setStateSet(shadowStateSet.get());
    osg::ref_ptr<osg::Geode> shadowGeode = new osg::Geode;
    shadowGeode->addDrawable( shadowQuad.get() );

    shadowTransf->addChild(shadowGeode);
    shadowTransf->setPosition(osg::Vec3(0,-100,2));
    shadowTransf->setDataVariance( osg::Object::DYNAMIC );
    shadowTransf->setScale(osg::Vec3(8,8,2));

    markerTransf->addChild(markerNode);
    markerTransf->setScale(osg::Vec3(.05,.05,.05)); // 0.05
    markerTransf->setPosition(osg::Vec3(0,-100,5)); /// 18.7859 y -153.89 z 40.8983           0,-100,5
    markerTransf->setAttitude(osg::Quat(0.0f, osg::Y_AXIS, 0.0f, osg::Z_AXIS, 0.0f, osg::X_AXIS));
    markerTransf->setDataVariance( osg::Object::DYNAMIC );

    static_cast<osg::Node*>(markerTransf)->setUpdateCallback( new MarkerCallback(shadowTransf.get()) );

}



void ARHelper::detectCollisions(osg::ref_ptr<osg::PositionAttitudeTransform> ballTransf,
                      pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr kdtree,
                      bool *collidedLeft, bool *collidedRight, bool *collidedFront, bool *collidedBack, bool *collidedBelow)
{

    std::vector<int> search_indexes_front;
    std::vector<int> search_indexes_back;
    std::vector<int> search_indexes_left;
    std::vector<int> search_indexes_right;
    std::vector<float> search_radiuses;

    // Gets ball coordinates in PCL measure
    pcl::PointXYZRGBA ball;   

    ball.x = -ballTransf->getPosition().x()/100.f;   

    ball.y = -ballTransf->getPosition().z()/100.f;   

    ball.z = -ballTransf->getPosition().y()/100.f;

    // Detecção de obstáculos à esquerda
    
    //47. Vamos começar por procurar um ponto que se encontre à esquerda da bola
    // e um pouco mais acima do seu centro (para evitar detectar o chão). este ponto 
    // representará a zona one vamos procurar obstáculos à esquerda
    pcl::PointXYZRGBA ballNeighborPoint;
    ballNeighborPoint.x = ball.x - 0.05;   
    ballNeighborPoint.y = ball.y - 0.05;   
    ballNeighborPoint.z = ball.z;
   
    pcl::PointXYZRGBA ballNeighborPointRight;
    ballNeighborPointRight.x = ball.x + 0.05;   
    ballNeighborPointRight.y = ball.y - 0.05;   
    ballNeighborPointRight.z = ball.z;
   
    pcl::PointXYZRGBA ballNeighborPointFront;
    ballNeighborPointFront.x = ball.x;   
    ballNeighborPointFront.y = ball.y - 0.05;   
    ballNeighborPointFront.z = ball.z;
   
    pcl::PointXYZRGBA ballNeighborPointBack;
    ballNeighborPointBack.x = ball.x;   
    ballNeighborPointBack.y = ball.y + 0.05;   
    ballNeighborPointBack.z = ball.z + 0.05;

    //48. Pesquisa pelos vizinhos que se encontram perto da região de análise (raio de 0.05)
    //    guarda os vizinhos em search indexes
    kdtree->radiusSearch (ballNeighborPoint, 0.05, search_indexes_left, search_radiuses);       
    *collidedLeft = !(search_indexes_left.size() == 0);   

    kdtree->radiusSearch (ballNeighborPointRight, 0.05, search_indexes_right, search_radiuses);       
    *collidedRight = !(search_indexes_right.size() == 0);   
    
    kdtree->radiusSearch (ballNeighborPointFront, 0.05, search_indexes_front, search_radiuses);       
    *collidedFront = !(search_indexes_front.size() == 0);   
    
    kdtree->radiusSearch (ballNeighborPointFront, 0.05, search_indexes_back, search_radiuses);       
    *collidedBack = !(search_indexes_back.size() == 0); 

}

// Traces a path between robot and marker, and draws a line if no obstacles detected
// We search for obstacles only in the objects cloud to ease the process and avoid detecting floor for example, and for efficiency purposes
// Also, for a matter of debugging, we can by this log the object detected
// The detection is based on parametric lines: R(t) = (1-t)*C + t*P. C is the marker center and P the robot center
bool ARHelper::traceMarkerPathToRobot(osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf, osg::ref_ptr<osg::PositionAttitudeTransform> tracepathGroupTransf, float* distance){
    // Gets the index of objects in buffer
    int currentCloudObjsIndex = this->classifiedObjectsBuffer.size()-1;
    std::vector<DetectionHelper::ClassifiedObject> classifiedObjects = this->classifiedObjectsBuffer[currentCloudObjsIndex];
    bool foundObject = false;
    // Gets robot
    int robotIndex = getRobotIndex();
    *distance = 0;
    // Cloud may not have robot
    if(robotIndex>0){
        DetectionHelper::ClassifiedObject robot = classifiedObjects[robotIndex];
        pcl::PointXYZRGBA robotCenter = robot.centerOfMassAndMeanColor;

        // Gets the marker position in PCL coordinates
        pcl::PointXYZRGBA markerPos;   
        markerPos.x = -markerTransf->getPosition().x()/100.f;   
        markerPos.y = -markerTransf->getPosition().z()/100.f; 
        markerPos.z = -markerTransf->getPosition().y()/100.f;

        //Levels the robot center with the marker height
        robotCenter.y=markerPos.y;

        // Gets the orientation of the trace path
        osg::Vec3 traceOrientation = pclToOsg(robotCenter)-pclToOsg(markerPos);
        float distanceOSG = traceOrientation.length();
        *distance = distanceOSG;
        traceOrientation.normalize();
        // Determines yaw of the trace
        double traceYaw = atan2(traceOrientation.y(),traceOrientation.x());

        float t = 0;
        //Iterates over the parametric line
        while(1-t>=0 && !foundObject){
            // Computes R(t) = (1-t)*C+t*P
            pcl::PointXYZRGBA rpt = DetectionHelper::sumPoints(DetectionHelper::scalePoint(markerPos, 1-t), DetectionHelper::scalePoint(robotCenter, t));
            osg::Vec3 rptOsg = pclToOsg(rpt); 

            // Iterates over all classified objects to find a collision
            for(int i=0; i<classifiedObjects.size() && !foundObject; i++){
                // except the robot of course
                if(i!=robotIndex){
                    // Gets sceneObject
                    DetectionHelper::ClassifiedObject sceneObject = classifiedObjects[i];
                    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sceneObjCloud = sceneObject.cloud;

                    // Makes a radius search around R(t)
                    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr kdtree (new pcl::search::KdTree<pcl::PointXYZRGBA>);   
                    kdtree->setInputCloud (sceneObjCloud);
                    std::vector<int> search_indexes;
                    std::vector<float> search_radiuses;

                    kdtree->radiusSearch (rpt, 0.05, search_indexes, search_radiuses); 

                    foundObject = (search_indexes.size() > 0);

                    if(foundObject){
                        std::cout << "There is an object of type " << sceneObject.classification << " on the way " << std::endl;
                    }
                }
            }
            
            t+=0.05;
        }

        
        float tStep = rulerWidth/distanceOSG;
        
        // Todo: replace sequence of ruler tapes with a unique geode with texture tiling
        // Add another loop to add the ruler. We do this separately from the previous loop
        // Because this way we guarantee a coherent ruler, without affecting the kdtree precision
        t=tStep;
        while(1-t>0 && !foundObject){
            // Computes R(t) = (1-t)*C+t*P
            pcl::PointXYZRGBA rpt = DetectionHelper::sumPoints(DetectionHelper::scalePoint(markerPos, 1-t), DetectionHelper::scalePoint(robotCenter, t));
            osg::Vec3 rptOsg = pclToOsg(rpt); 
            //std::cout << "Result of R(t)=(1-t)C+tP => x=" << rpt.x << " y= " << rpt.y << " z="<< rpt.z << endl;

            // Creates a trace
            osg::ref_ptr<osg::PositionAttitudeTransform> traceTransf = new osg::PositionAttitudeTransform;  
            traceTransf->setPosition(rptOsg);
            traceTransf->addChild(ruler);

            osg::Quat traceAttitude( 0, osg::X_AXIS, 0, osg::Y_AXIS, traceYaw, osg::Z_AXIS ); //applies a roll (to "lie" the capsule on the floor), and a yaw, corresponding to the estimated robot orientation
            traceTransf->setAttitude(traceAttitude);

            tracepathGroupTransf->addChild(traceTransf);


            t+=tStep;
        }


    }
    return !foundObject;
}



osg::ref_ptr<osg::Geode> ARHelper::createTextureGeode(float width, float length, const std::string &imagePath){

    osg::ref_ptr<osg::Shader> vertShader2 =
    new osg::Shader( osg::Shader::VERTEX, markerVertexSource );
    osg::ref_ptr<osg::Shader> fragShader2 =
    new osg::Shader( osg::Shader::FRAGMENT, markerFragmentSource );
    osg::ref_ptr<osg::Program> program2 = new osg::Program;
    program2->addShader( fragShader2.get() );
    program2->addShader( vertShader2.get() );

    // the four vertexes defining the ground plane
    osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array;
    vertices->push_back( osg::Vec3(width/2, length/2, 0.0f));
    vertices->push_back( osg::Vec3(-width/2, length/2, 0.0f));
    vertices->push_back( osg::Vec3(-width/2, -length/2, 0.0f));
    vertices->push_back( osg::Vec3(width/2, -length/2, 0.0f));

    // the texture coordinates for each vertex
    osg::ref_ptr<osg::Vec2Array> texcoords = new osg::Vec2Array;
    texcoords->push_back( osg::Vec2(0.0f, 0.0f) );
    texcoords->push_back( osg::Vec2(0.0f, 1.0f) );
    texcoords->push_back( osg::Vec2(1.0f, 1.0f) );
    texcoords->push_back( osg::Vec2(1.0f, 0.0f) );

    // the texture to be mapped to the quadrilateral
    osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D;
    osg::ref_ptr<osg::Image> image = osgDB::readImageFile( imagePath );
    texture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
    texture->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
    texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
    texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
    texture->setImage( image.get() );

    // seting the normal vector for the quadrilateral
    osg::ref_ptr<osg::Vec3Array> normals = new osg::Vec3Array;
    normals->push_back( osg::Vec3(0.f, 0.f, 1.f) );

    // defining the quadrilateral, given its vertexes, normal, material, and texture coordinates
    osg::ref_ptr<osg::Geometry> quad = new osg::Geometry;
    quad->setVertexArray( vertices.get() );
    quad->setNormalArray( normals.get() );
    // stating that the normal vector is employed on the four vertexes
    quad->setNormalBinding( osg::Geometry::BIND_OVERALL );
    quad->setTexCoordArray( 0, texcoords.get() );
    // stating tha the geometry is a quad with the vertexes indexed between position 0 and 4 of array 'vertices'
    quad->addPrimitiveSet( new osg::DrawArrays(GL_QUADS, 0, 4) );

    // creating the geode that will encapsulate the just created texture
    osg::ref_ptr<osg::Geode> ground = new osg::Geode;
    ground->addDrawable( quad.get() );

    // we need to update the StateSet to include the texture information
    ground->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture.get(), osg::StateAttribute::ON );

    ground->getOrCreateStateSet()->setMode( GL_LIGHT1, osg::StateAttribute::ON );
    ground->getOrCreateStateSet()->setAttributeAndModes( program2.get() );
 

    // return the transformation node
    return ground;
}


// Creates an HUD. Based on lab 4
osg::ref_ptr<osg::Camera> ARHelper::createHUD(){

    // the geode responsible for storing the text (which is a form of geometry)
    osg::ref_ptr<osg::Geode> textGeode = new osg::Geode;
    // let us associate the text to the geode (the text is created in other function; check it out
    textGeode->addDrawable( createText(osg::Vec3(0.0f, 565.0f, 0.0f),"Robot collision evaluator", 15.0f) );
    textGeode->addDrawable( createText(osg::Vec3(0.0f, 550.0f, 0.0f),"Movement controls: i,j,k,l", 15.0f) );
    textGeode->addDrawable( createText(osg::Vec3(0.0f, 535.0f, 0.0f),"Image mode controls: a(normal), s(shading from robot), d(risk)", 15.0f) );
    textGeode->addDrawable( createText(osg::Vec3(0.0f, 520.0f, 0.0f),"Toogle marker collisions ON/OFF: q", 15.0f) );
 

    // the second text will be made dynamic, so we use a class member "textFrames" to store it
    distanceTextHudFrame = createText(osg::Vec3(0.0f, 5.0f, 0.0f), "Distance to robot: n/a", 20.0f);
    // associating this new text to the geode
    textGeode->addDrawable(distanceTextHudFrame);

    // calling a function (check it out) to create an orthographic camera, which will be rendered on the top of the viewing camera.
    // this camera will have as its single child the geode we've just created; this way, this camera will only see the two texts.
    osg::ref_ptr<osg::Camera> hudCamera = createHUDCamera(0, 640, 0, 580);
    // attaching the text geode to the camera
    hudCamera->addChild( textGeode.get() );
    // stating that there should be no illumination effects associated to this camera (we just want to see the text with flat shading)
    hudCamera->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF );

    // return the camera object to be later on added to the scene graph
    return hudCamera;
}

// Creates the camera for HUD. Based on lab 4
osg::ref_ptr<osg::Camera> ARHelper::createHUDCamera( double left, double right, double bottom, double top ){
    osg::ref_ptr<osg::Camera> camera = new osg::Camera;
    // stating that this camera has a global position and, thus, not changeable by the transform hierarchy
    camera->setReferenceFrame( osg::Transform::ABSOLUTE_RF );
    camera->setClearMask( GL_DEPTH_BUFFER_BIT );
    // the next property ensures that the data rendered by this camera is overlaid on the output of other cameras
    camera->setRenderOrder( osg::Camera::POST_RENDER, 1);
    camera->setAllowEventFocus( false );
    // stating that this camera is orthographic
    camera->setProjectionMatrix(osg::Matrix::ortho2D(left, right, bottom, top) );
    return camera;
}

// this function creates a text object, that will be used for the HUD
osgText::Text* ARHelper::createText( const osg::Vec3& pos, const std::string& content, float size ){
    osg::ref_ptr<osgText::Font> g_font = osgText::readFontFile("fonts/arial.ttf");
    osg::ref_ptr<osgText::Text> text = new osgText::Text;
    text->setFont( g_font.get() );
    text->setCharacterSize( size );
    text->setAxisAlignment( osgText::TextBase::XY_PLANE );
    text->setPosition( pos );
    text->setText( content );
    // tell the system that the texto will be changing
    text->setDataVariance(osg::Object::DYNAMIC);
    return text.release();
}
