#include "MarkerController.h"

//*************************************
// Based on Lab5. Just changed class name
//*************************************

// Processes keyboard events for ball controll
bool MarkerController::handle( const osgGA::GUIEventAdapter& ea,
                            osgGA::GUIActionAdapter& aa )
{
    if ( !_marker )
        return false;
    
    //39. _marker é a transformada que permite controlar a bola
    osg::Vec3 v = _marker->getPosition();
    osg::Quat q = _marker->getAttitude();

    switch ( ea.getEventType() )
    {
            
        case osgGA::GUIEventAdapter::KEYDOWN:
            switch ( ea.getKey() )
        {
            // Marker controls
            // Left 
            case 'j': case 'J':
                //40.
                moved=true;
                if (!(*collidedLeft && toogleCollisions)){
                    v.x() += 1.0;                   
                    q *= osg::Quat(0.2, osg::Y_AXIS, 0.0, osg::X_AXIS, 0.0, osg::Z_AXIS);               
                }

                break;
            case 'l': case 'L':
                moved=true;
                if (!(*collidedRight && toogleCollisions)){
                    v.x() -= 1.0;  // Right               
                    q *= osg::Quat(-0.2, osg::Y_AXIS, 0.0, osg::X_AXIS, 0.0, osg::Z_AXIS); // roda a bola para a direita              
                }
                break;
            // Front   
            case 'i': case 'I':
                //41.
                moved=true;
                if (!(*collidedFront && toogleCollisions)){                   
                    v.y() -= 1.0;                   
                    q *= osg::Quat(0.2, osg::X_AXIS, 0.0, osg::Y_AXIS, 0.0, osg::Z_AXIS);               
                }
                break;
            // Back
            case 'k': case 'K':
                moved=true;
                if (!(*collidedBack && toogleCollisions)){                   
                    v.y() += 1.0;                   
                    q *= osg::Quat(-0.2, osg::X_AXIS, 0.0, osg::Y_AXIS, 0.0, osg::Z_AXIS);               
                }
                break;
            case 'q': case 'Q':
                toogleCollisions=!toogleCollisions;
                break;

            // IMAGE MODE CONTROLLS
            case 'a': case 'A':
                modeChanged = true;
                *imageMode = ARImageModes::NORMAL;
                break;
            case 'd': case 'D':
                modeChanged = true;
                *imageMode = ARImageModes::RISK;
                break;
            case 's': case 'S':
                modeChanged = true;
                *imageMode = ARImageModes::SHADING;
                break;
            default:
                break;
        }
            
            //42. Atualiza a posição da bola de acordo com as alterações resultantes dos eventos

            _marker->setPosition(v);

            _marker->setAttitude(q);

            break;
        default:
            break;
    }
    return false;
    
}


void MarkerCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{

    osg::PositionAttitudeTransform* markerTransf = static_cast<osg::PositionAttitudeTransform*>(node);
    osg::Vec3 v = markerTransf->getPosition();
    shadowTransf->setPosition(osg::Vec3(v.x(), v.y(), v.z()-3));
}