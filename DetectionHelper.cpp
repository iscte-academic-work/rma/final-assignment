#include "DetectionHelper.h"


const double DetectionHelper::robotHeight = 0.16;
const double DetectionHelper::robotWidth = 0.34;
const double DetectionHelper::robotHeightLowerTolerance = .10;
const double DetectionHelper::robotWidthLowerTolerance = .05;

const double DetectionHelper::robotHeightHigherTolerance = .10;
const double DetectionHelper::robotWidthHigherTolerance = .09;


DetectionHelper::DetectionHelper(){}
DetectionHelper::~DetectionHelper(){}
void DetectionHelper::easyLog(std::string msg){
    std::cout << msg << std::endl;
}

double DetectionHelper::distanceBetweenPoints(pcl::PointXYZRGBA point1, pcl::PointXYZRGBA point2){
    return sqrt(pow(point1.x-point2.x,2.0)+pow(point1.y-point2.y,2.0)+pow(point1.z-point2.z,2.0));
}

double DetectionHelper::distanceBetweenPointsOpenGL(pcl::PointXYZRGBA point1, pcl::PointXYZRGBA point2){
    return sqrt(pow(255.0*(point1.x+2-point2.x+2)/6.0,2.0)+pow(255.0*(point1.y+2-point2.y+2)/6.0,2.0)+pow(255.0*(point1.z-point2.z)/10.0,2.0));
}

void DetectionHelper::logMinMaxCenter(pcl::PointXYZRGBA minPt, pcl::PointXYZRGBA maxPt, pcl::PointXYZRGBA centerPt){ 
    std::cout<< "Min => x="<< minPt.x<<", y=" << minPt.y<<", z=" << minPt.z << " || r=" << ((minPt.rgba >> 16) & 0x0000ff)<<", g=" << ((minPt.rgba >> 8) & 0x0000ff) << ",b=" <<((minPt.rgba) & 0x0000ff) << ",a=" <<minPt.a << std::endl;
    std::cout<< "Max => x="<< maxPt.x<<", y=" << maxPt.y<<", z=" << maxPt.z << " || r=" << ((maxPt.rgba >> 16) & 0x0000ff)<<", g=" << ((maxPt.rgba >> 8) & 0x0000ff) << ",b=" <<((maxPt.rgba) & 0x0000ff) << ",a=" <<maxPt.a << std::endl;
    std::cout<< "Center => x="<< centerPt.x<<", y=" << centerPt.y<<", z=" << centerPt.z<< " || r=" << ((centerPt.rgba >> 16) & 0x0000ff)<<", g=" << ((centerPt.rgba >> 8) & 0x0000ff) << ",b=" <<((centerPt.rgba) & 0x0000ff) << ",a=" <<centerPt.a << std::endl;
    std::cout<<"(note: min and max got from getMinMax3D() method won't return any colour info)"<<std::endl;
}

// Saves a cloud in the execution output folder. CloudName must be given without extension
// Logs in terminal the cloud saved
void DetectionHelper::logCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, std::string cloudName, std::string description){
    pcl::PCDWriter writer;
    std::string cloudPath = CloudOutputFolder + cloudName + ".pcd";
    writer.write<pcl::PointXYZRGBA> (cloudPath, *cloud, false);

    std::cout<< "Saved cloud " << description << " at " << cloudPath << std::endl;
}


// Copies to cloud_out the points of cloud_in corresponding to the indexes. 
void DetectionHelper::copyToCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_out, pcl::PointIndices indexes){
    for(int i=0; i<indexes.indices.size(); i++){

        int index = indexes.indices[i];
        cloud_out->points.push_back(cloud_in->points[index]);
    }

    // For some reason we must redifine this, or when saving the cloud an error is thrown
    cloud_out->width = cloud_out->points.size();
    cloud_out->height = 1;
    cloud_out->is_dense = true; // this is kind of cheating, but we are saying there are no invalid points like inf or nan, but how it is guarantee?
}

// Projects cloud_in to cloud_out in XoZ. To do that we just put Y to 0 (lol)
void DetectionHelper::projectCloudToXZ(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_out){
    for(int i = 0; i < cloud_in->size(); i++){ // TODO : se tivermos tempo, olhar para a biblioteca boost. estes fors  podem ser processados em paralelo
        pcl::PointXYZRGBA point = cloud_in->points[i];
        point.y = 0; //haha (please forgive me for this funny momments. It is a stress relief)
        cloud_out->points.push_back(point);
    }

    // For some reason we must redifine this, or when saving the cloud an error is thrown
    cloud_out->width = cloud_out->points.size();
    cloud_out->height = 1;
    cloud_out->is_dense = true; // this is kind of cheating, but we are saying there are no invalid points like inf or nan, but how it is guarantee?
}

// Determines if cloud_in belongs to a bouding cylinder
// this bounding cylinder is defined by a radius, an height, a tolerance value (a perecentage of radius to be summed to the radius),
// and minProportion (minimal proportion of points of cloud_in that must belong to the cylinder premises)
bool DetectionHelper::cloudWithinBoundingCylinder(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA centerOfMass, double radius, 
                                                  double height, double radiusTolerance, double minProportion, bool verbose){

    int minPoints = cloud_in->size()*minProportion; // minimal number of points to be classified as belonging
    int numPoints = 0; // number of classified points
    radius += radius*radiusTolerance;
    double minY = centerOfMass.y-height/2;
    double maxY = centerOfMass.y+height/2;

    for(int i=0; i < cloud_in->size() && numPoints<=minPoints; i++){
        // To check if the point[i] of the cloud is withing the bounding circle, 
        // first we check if is within height premises (easy)
        // then we check if it is within a circumfrence at the height of this point, but with the x and z of the center of mass
        // to do that, we calculate the distance that goes from this heightened center to the point[i] and then we compare it to the radius
        pcl::PointXYZRGBA point = cloud_in->points[i];
        if(verbose)
            std::cout << "Is point with y=" << point.y << " inside height premises("<< minY << "," << maxY <<")? " << (point.y <= maxY && point.y >=minY) << std::endl;
        if(point.y <= maxY && point.y >=minY){
            // We put the center os mass in the same level as the cloud point in evaluation            
            pcl::PointXYZRGBA leveledCenter = centerOfMass;
            leveledCenter.y = point.y; 
            // determine the distance between the point in evaluation and the leveled center
            double distance = distanceBetweenPoints(leveledCenter, point);
            if(verbose)
                std::cout << "Is point in distance (" <<distance << ")radius (" <<radius << ")?"<< (distance <= radius) << std::endl;
            if(distance <= radius)
                numPoints+=1;
           // numPoints+= (distance <= radius); //spare the ifs. in c++ a boolen can be converted to int
        }

    }
    
    std::cout<< "At least " << numPoints << " points are withing the bounding cylinder"<< ". Minimum needed " << minPoints  << std::endl;

    return numPoints >= minPoints;
}

// Given a cloud we are going to find its max distance in the plane XoZ. projectedCloudName is just for logging purposes
// numSearchIterations defines how many times we will search for the most distant points
// must give value bigger than two. automatically will set to three is lower is passed
// If extended is true this will make a more heavy search for XoZ distance, using ransac to get multiple surfaces, and extract the maximum distance. 
// This can be useful when the object has missing parts that may cause a shift on its centroid, and so we just want to use it's top surface to further calculations
// In this algorithm we also compare the value got from the "ransac's" with the default value got from maxDistance. So minProportion, means that the maxDistance found
// with RANSAC must be at least minProportion*maxDefaultDistance to be considered. Else we will return the maxDefaultDistance
//
// When this consideration is made, shiftedCM will be set to true (meaning that the center of mass must be computed and top_surface_cloud has points)
double DetectionHelper::findXoZMaxDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr top_surface_cloud, std::string projectedCloudName, int numSearchIterations, bool extended, double minProportion, bool &shiftedCM)
{ 
    shiftedCM = false;
    double valueComputed = 0;
    double maxDistanceDefault = findXoZMaxDistance(cloud_in, projectedCloudName, numSearchIterations);
    valueComputed = maxDistanceDefault;
    if(extended){
        bool stop = false;
        int cloudSize = cloud_in->size();
        int tempCloudMinSize = cloudSize/5; // we set to 5 because a cube has 6 faces, but only 5 visible.
        double maxDistance = 0;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr temp_cloud = cloud_in;
        // In each iteration applies RANSAC to get the dominant surface of temp_cloud
        // temp_cloud starts as the cloud_in but after each iteration will have points removed, corresponding to the surface got from ransac
        // the surface got will be used to compute max distance, so it will be subject also to XoZ projection. 
        int i = 0;
        //pcl::PointCloud<pcl::PointXYZRGBA>::Ptr selected_top_surface_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
        while(!stop){
            pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
            pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
            pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
            //Gets coeffs and inliers by applying RANSAC
            std::cout<<"RANSACing " << temp_cloud->size() << " points." << std::endl;
            getDominantPlane(seg, temp_cloud, coefficients, inliers, 0.005);       
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane (new pcl::PointCloud<pcl::PointXYZRGBA>);
            // Extracts the surface
            extractCloud(temp_cloud, cloud_on_plane, inliers, false); 
            // Extracts the rest of the object for the next iteration
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_off_plane (new pcl::PointCloud<pcl::PointXYZRGBA>);
            extractCloud(temp_cloud, cloud_off_plane, inliers, true);
            // First we evaluate if the clouds are not too small
            if(cloud_off_plane->size() < tempCloudMinSize || cloud_on_plane->size() < tempCloudMinSize){
                stop=true;
            }
            else{
                // Then we update the max distance and top surface cloud, if it is to be updated
                double maxDist = findXoZMaxDistance(cloud_on_plane, projectedCloudName+"_ransac_"+std::to_string(i), numSearchIterations);
                if(maxDist>maxDistance){
                    maxDistance = maxDist; 
                    // This had to be done, because we want to read back top_surface_cloud after the function call. 
                    // If we did this without the "*" we would be setting the memory address of top_surface_cloud to the cloud_on_plane one, which is not desired
                    *top_surface_cloud = *cloud_on_plane; 
                }

            }

            temp_cloud = cloud_off_plane;
            i++;
        }

        std::cout << " Got for " << projectedCloudName << " a ransac based maxDistance of " << maxDistance << " and a default maxDistance of " << maxDistanceDefault << " being minProportionDistance " << (maxDistanceDefault * minProportion) << std::endl;
        // We compare the maxDistance computed using different ransacs, with the default value
        // We do this because we do not want to consider a value that its way shorter
        easyLog("what the hell 1");
        if(maxDistance >= (maxDistanceDefault * minProportion))
        {
            valueComputed = maxDistance;
            shiftedCM = true;
        }
        easyLog("what the hell 2");
        
    }

    return valueComputed;
}

// Given a cloud we are going to find its max distance in the plane XoZ. projectedCloudName is just for logging purposes
// numSearchIterations defines how many times we will search for the most distant points
// must give value bigger than two. automatically will set to three is lower is passed
double DetectionHelper::findXoZMaxDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, std::string projectedCloudName, int numSearchIterations){
    //First we project to XoZ
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr projected_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);

    projectCloudToXZ(cloud_in, projected_cloud);

    logCloud(projected_cloud, projectedCloudName, "that shows a cloud projected in XoZ");

    // To calculate the maximum distance we are going to do the following
    // We grab a random point (lets say, its first). 
    // We use KdTree to determine the most distant point. Then we find again the most distant point to that
    // And do it again for the last point. Then we calculate the distance between this two points.
    // If we only have square, rectangle, circular (and maybe more shapes, this are the ones I can think about),
    // We only need to do it two times. But in case of triangles and others we need to do more iterations to make sure.
    // Maybe we configure a number of iterations in the function. Lets see
    numSearchIterations = numSearchIterations < 3 ? 3:numSearchIterations;
    pcl::PointXYZRGBA point1 = projected_cloud->points[0];
    pcl::PointXYZRGBA point2 = projected_cloud->points[0];

    // And here I go again on my own
    // going down the only search method I've ever known
    // like a programmmer I was born to code alone \m/
    pcl::search::KdTree<pcl::PointXYZRGBA> kdtree;
    kdtree.setInputCloud (projected_cloud);
    
    for(int i=0; i<numSearchIterations; i++){
        point1=point2;
        std::vector<int> pointIdxSearch;
        std::vector<float> pointSquaredDistance;
        // we set for a very large radius because we want to get everything
        kdtree.radiusSearch (point1, 2, pointIdxSearch, pointSquaredDistance);
        //pointIdxSearch contains all the point indexes in crescent order of closeness to point1
        int farthestIndex = pointIdxSearch[pointIdxSearch.size()-1];
        point2 = projected_cloud->points[farthestIndex];
    }

    return distanceBetweenPoints(point1, point2);    
}
 
// Finds the farthest point in a cloud, from a given point. We are assuming the cloud has not more than 10m wide
pcl::PointXYZRGBA DetectionHelper::findMostDistantPoint(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA point){
    pcl::search::KdTree<pcl::PointXYZRGBA> kdtree;
    kdtree.setInputCloud (cloud_in);

    pcl::PointXYZRGBA farthestPoint;
    std::vector<int> pointIdxSearch;
    std::vector<float> pointSquaredDistance;
    
    kdtree.radiusSearch (point, 30, pointIdxSearch, pointSquaredDistance);
    int farthestIndex = pointIdxSearch[pointIdxSearch.size()-1];
    farthestPoint = cloud_in->points[farthestIndex];

    return farthestPoint;
}

// Checks if a cloud belongs to a plane, based on its coefficients (a,b,c,d)
// A plane eq is defined by ax+by+cz+d=0. 
// We apply this equation for each point, and if results in a interval of  [-tolerance, tolerance], the point is classified as belonging to place
// If a minimum proportion of points belongs, this cloud is classified as belonging to plane
bool DetectionHelper::belongsToPlane(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, double a, double b, double c, double d, double tolerance, float minProportion){
    int minPoints = cloud_in->size()*minProportion;
    int numClassified = 0;
    tolerance = abs(tolerance);
    bool belongs = false;
    // To avoid iterate all points, when we reach the mininum points needed to be classified, we return
    for(int i=0; i<cloud_in->size() && numClassified <= minPoints; i++){
        pcl::PointXYZRGBA cloudPoint = cloud_in->points[i];
        double planeEqRes = a*cloudPoint.x + b*cloudPoint.y + c*cloudPoint.z +d;
        // If the result of the plane equation is aproximately 0, we consider this point as belonging
        if(planeEqRes <= tolerance && planeEqRes >= -tolerance)
            numClassified++;
    }
    belongs = numClassified >= minPoints;
    return belongs;
}


// Gets the normalized values of a,b,c,d from coefficients of ax+by+cz+d=0 equation
void DetectionHelper::normalizePlaneCoeffs(pcl::ModelCoefficients::Ptr coefficients, double *a, double *b, double *c, double *d){
    double norm = sqrt(pow(coefficients->values[0],2.0) + pow(coefficients->values[1],2.0)+ pow(coefficients->values[2],2.0));
    *a = coefficients->values[0]/norm;
    *b = coefficients->values[1]/norm;
    *c = coefficients->values[2]/norm;
    *d = coefficients->values[3]; 
}

// Receives the cloud name and creates a folder inside BaseCloudsFolder
void DetectionHelper::createCloudOutputFolder(std::string cloudName){
    CloudOutputFolder = BaseCloudsFolder+cloudName+"/";
    mkdir(CloudOutputFolder.c_str(),0777);

}

// Creates a folder where all the clouds will be logged. Path will be saved in BaseCloudsFolder
void DetectionHelper::createExecutionOutputFolder(){
    char* cloudFolderName =(char*)calloc(80,sizeof(char));
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(cloudFolderName,80,"%Y-%m-%d %H:%M:%S",timeinfo);
    std::string str(cloudFolderName);
    std::cout<< cloudFolderName << std::endl;

    char fullOutCloudDirectory[strlen(cloudFolderName)+strlen(CLOUD_OUTPUT_BASE_FOLDER)];
    strcpy(fullOutCloudDirectory, (char*)CLOUD_OUTPUT_BASE_FOLDER);
    strcat(fullOutCloudDirectory,cloudFolderName);

    mkdir(fullOutCloudDirectory,0777);

    BaseCloudsFolder = fullOutCloudDirectory;
    BaseCloudsFolder = BaseCloudsFolder+"/";
}

// Gets dir content
int DetectionHelper::getdir (std::string dir, std::vector<std::string> &files, std::string mustContainString)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        std::string item = std::string(dirp->d_name);
        if (item.find(mustContainString) != std::string::npos){        
            files.push_back(std::string(dirp->d_name));
        }

    }
    closedir(dp);
    return 0;
}

// Reads cloud from file and creates a folder for its outputs
int DetectionHelper::loadCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, std::string cloudName){
    createCloudOutputFolder(cloudName);
    std::string cloudPath = CloudInputFolder + cloudName + ".pcd";
    CloudInAnalysis = cloudName;
    if (pcl::io::loadPCDFile (cloudPath, *cloud) == -1)  {
        PCL_ERROR ("Couldn't read the PCD file \n");
        return (-1);
    }
    std::cout << "Read " << cloudPath << std::endl;
    return 0;
}


void DetectionHelper::voxeliseCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in,
                      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised){
    pcl::VoxelGrid<pcl::PointXYZRGBA> voxel_grid;
    voxel_grid.setInputCloud (cloud_in);
    voxel_grid.setLeafSize (0.01, 0.01, 0.01);
    voxel_grid.filter (*cloud_voxelised);    

    std::cout << "Applied voxelization with previous size " << cloud_in->points.size() << " and new size " << cloud_voxelised->points.size() << std::endl;
}

// OBSOLETE.
// Estimate the standard deviation of distances in a cloud. 
float DetectionHelper::estimateStdDevDistance(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, int Kneighbors){
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised (new pcl::PointCloud<pcl::PointXYZRGBA>);
    voxeliseCloud(cloud_in,cloud_voxelised);
    std::cout<< "Cloud for mean distance estimation with " << cloud_voxelised->points.size() << " points" << endl;


    // In this loop we go through everypoint in the cloud 
    // and use kdtree to search for neighbours. For the first Kneighbors we 
    // determine the std deviation of distances
    float meanStdDev = 0;
    int n = cloud_voxelised->points.size() / 5000;
    for(int i = 0; i < n; i++){
        pcl::search::KdTree<pcl::PointXYZRGBA> kdtree;
        kdtree.setInputCloud (cloud_voxelised);
        std::vector<int> pointIdxSearch;
        std::vector<float> pointSquaredDistance;

        kdtree.radiusSearch (cloud_voxelised->points[i], 0.1, pointIdxSearch, pointSquaredDistance);
        
        // We are not considering all neighbors for the mean count
        // That is because we are going to use the determined value for outlier removal, and 
        // we must set with the same neighbor count
        int neighbors = pointIdxSearch.size() < Kneighbors ? pointIdxSearch.size() : Kneighbors;
        float sum = 0;
        float stdDev = 0;
        if(neighbors!=0){     
            for(int j=0; j < neighbors; j++){
                float distance = pointSquaredDistance[j];
                distance = isnan(distance) ? 0 :distance; 
                sum+=distance;
            }
            float temp_mean = sum/neighbors;
            
            //std::cout<< "temp mean" << temp_mean << std::endl;

            // Now we compute the std deviation of distances based on https://www.todamateria.com.br/desvio-padrao/
            
            for(int j=0;j<neighbors; j++){
                stdDev += pow(pointSquaredDistance[j]-temp_mean,2);
            }
            stdDev = sqrt(stdDev/neighbors);
        } 
        meanStdDev += stdDev;
    }

    std::cout<<"Estimated an stdDev of " << meanStdDev<< std::endl;

    std::cout<<"Estimated an stdDev of " << meanStdDev/n << std::endl;

    return meanStdDev/n;
}


// Returns a PointXYZRGBA containing the center of mass in the depth channels and the mean color in the color chanels
void DetectionHelper::estimateCenterOfMass(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointXYZRGBA &centerOfMass){
    pcl::PointXYZRGBA p_mean;
    p_mean.x=0;
    p_mean.y=0;
    p_mean.z=0;
    p_mean.r=0;
    p_mean.g=0;
    p_mean.b=0; 

    int redMean = 0;
    int greenMean = 0;
    int blueMean = 0;


    int cloudSize = cloud_in->size(); // to prevent division by zero errors. the result will be zero anyways

    if(cloudSize > 0){
        for(int i=0; i<cloudSize;i++){
            pcl::PointXYZRGBA point = cloud_in->points[i];
            //r,g,b saves the color channels in an hexadecimal string. To determinate the mean we need to convert to decimal,
            // To do so we need to unpack the colors as instructed in here http://docs.pointclouds.org/1.7.0/structpcl_1_1_point_x_y_z_r_g_b_a.html#a5b923a261013f4eb20aadcef2c405e23
            p_mean.x+=point.x;
            p_mean.y+=point.y;
            p_mean.z+=point.z;
            redMean += ((point.rgba >> 16) & 0x0000ff);
            greenMean += ((point.rgba >> 8) & 0x0000ff);
            blueMean += ((point.rgba) & 0x0000ff);
        }
        
        redMean = redMean/cloudSize;
        greenMean = greenMean/cloudSize;
        blueMean = blueMean/cloudSize;
        // We don't need to conver each r,g,b to hex. We just pack it all again
        int rgba = ((int)redMean) << 16 | ((int)greenMean) << 8 | ((int)blueMean);
        p_mean.rgba=rgba; 
        p_mean.x=p_mean.x/cloudSize;
        p_mean.y=p_mean.y/cloudSize;
        p_mean.z=p_mean.z/cloudSize;
    }
    centerOfMass = p_mean;    
}

// Applies RANSAC to estimate plane coeffs and inliers
void DetectionHelper::getDominantPlane(pcl::SACSegmentation<pcl::PointXYZRGBA> seg, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers, double distanceThreshold){
    // setOptimizeCoefficients -> consider applying this in situations where we know we will have outliers
    seg.setModelType (pcl::SACMODEL_PLANE); //Defines that a plane will be the base model for the coeffs
    seg.setMethodType (pcl::SAC_RANSAC);    //Sets the method as RANSAC
    //seg.setMaxIterations (10000);           //Sets a limit of iterations. If slower, reduce this
    seg.setDistanceThreshold (distanceThreshold);        //Maximal distance the points need to be to be considered
    seg.setInputCloud (cloud_in);       
    seg.segment (*inliers,*coefficients); // Estimates the coefs a,b,c,d from ax+by+cz+d and normalizes them (by dividing them by their magnitude)
}


// Applies RANSAC to estimate plane coeffs and inliers
void DetectionHelper::getDominantPlane(pcl::SACSegmentation<pcl::PointXYZRGBA> seg, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers){
    getDominantPlane(seg,cloud_in,coefficients,inliers, 0.025d);
}

// Determines if an object is a robot based on its dimensions
bool DetectionHelper::isRobot(double objectWidth, double objectHeight){
    double absoluteWidthLowerTolerance = robotWidthLowerTolerance*robotWidth;
    double absoluteHeightLowerTolerance = robotHeightLowerTolerance*robotHeight;

    double absoluteWidthHigherTolerance = robotWidthHigherTolerance*robotWidth;
    double absoluteHeightHigherTolerance = robotHeightHigherTolerance*robotHeight;


    double minWidth = robotWidth - absoluteWidthLowerTolerance;
    double maxWidth = robotWidth + absoluteWidthHigherTolerance;
    double minHeight = robotHeight - absoluteHeightLowerTolerance;
    double maxHeight = robotHeight + absoluteHeightHigherTolerance;

    return (objectHeight >= minHeight && objectHeight <= maxHeight) && (objectWidth >= minWidth && objectWidth <= maxWidth);
}

// An object is a potential robot if at least has the required width
bool DetectionHelper::isPotentialRobot(double objectWidth){
    double absoluteWidthLowerTolerance = robotWidthLowerTolerance*robotWidth;
    double absoluteWidthHigherTolerance = robotWidthHigherTolerance*robotWidth;
    double minWidth = robotWidth - absoluteWidthLowerTolerance;
    double maxWidth = robotWidth + absoluteWidthHigherTolerance;
    return (objectWidth >= minWidth && objectWidth <= maxWidth);
}

// Estimates the camera pose by first extacting a portion of the cloud that is right in front of camera. Then uses RANSAC to get the 
// Plane coeffs of that cloud portion and calculates the pose params (pitch, roll, height) based on the determined coeffs
void DetectionHelper::estimateCameraPose(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, double *pitch, double *roll, double *height)
{    
    // This point cloud will save the portion of screen that is right in front of camera
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_centre (new pcl::PointCloud<pcl::PointXYZRGBA>);

    cloud_centre->height=1; int step = 2; long index;
    
    // We are assuming that the floor is right in front of the camera
    //percorre linhas e colunas da área directamente em frente à camera com o objectivo de estimar a sua pose.
    // assim sendo é obtido o cloud center
    std::cout<< "Going to read " << cloud_in->height * cloud_in->width << " points" << std::endl;
    for (int row=0;row<cloud_in->height/4;row+=step){
        for (int col=cloud_in->width/2-50;col<cloud_in->width/2+50;col+=step){
            index = (cloud_in->height-row-1)*cloud_in->width + col; //este indice é construido com base na linha e coluna em que está tudo presente
            cloud_centre->points.push_back(cloud_in->points[index]);
            cloud_centre->width++;
        }
    }
    
    std::cout<< "Cloud centre with " << cloud_centre->points.size() << " points" << endl;
    // Saves the created cloud in the passed dir 
    logCloud(cloud_centre, CloudInAnalysis + "_centre", "used for pose estimation");

    // Determines the coefficients that best describes the plane equation (ax+by+cz+d)
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

    // TODO : Existem aqui valores martelados. Talvez colocar em configuração à parte
    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
    // setOptimizeCoefficients -> consider applying this in situations where we know we will have outliers
    getDominantPlane(seg, cloud_centre, coefficients,inliers); //uses RANSAC to determine the coeffs and inliers

    double norm = sqrt(pow(coefficients->values[0],2.0) + pow(coefficients->values[1],2.0)+ pow(coefficients->values[2],2.0));
    double c_a,c_b,c_c,c_d; 

    // normalizes and extracts a,b,c,d coeffs 
    normalizePlaneCoeffs(coefficients, &c_a, &c_b,&c_c,&c_d);

    std::cout << "Coefficients a: " << c_a << " b: " << c_b << " c: " << c_c << " d: " << c_d << "." << std::endl;

    // TODO tentar perceber melhor o motivo disto
    //8.A. inversoes de sinal a ter em conta
    if(c_c<0){
        c_a*=-1;c_b*=-1;c_c*=-1;
    }
    
    //8. Com os coeficientes obtidos podemos então determinar a inclinação (pitch e roll) e altura (rever slides teóricos sobre isto)
    *pitch = asin(c_c);
    *roll = -acos(c_b/cos(*pitch));
    *height=fabs(c_d);

    //8.B.
    if(c_a<0)
        (*roll) *= -1;


    // Logs the values
    std::cout << "Camera pitch: " << *pitch * 180/M_PI << " [deg]; Camera roll: " << *roll * 180/M_PI << " [deg]." << std::endl;

    std::cout << "Camera height: " << *height << std::endl;

}

void DetectionHelper::rotatePointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in,
                      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated, 
                      double camera_pitch, double camera_roll, double camera_height)
{
    std::cout<< "Going to rotate point cloud " << CloudInAnalysis << std::endl;

    //12. Aplica transformações à cloud voxelizada de forma a alinhar a mesma com o sensor
    Eigen::Affine3f t1 = pcl::getTransformation(0.0, -camera_height, 0.0, 0.0,0.0, 0);
    Eigen::Affine3f r1 = pcl::getTransformation (0.0, 0.0, 0.0, -camera_pitch, 0.0, 0.0);
    Eigen::Affine3f r2 = pcl::getTransformation (0.0, 0.0, 0.0, 0.0, 0.0, -camera_roll);

    pcl::transformPointCloud(*cloud_in, *cloud_rotated, t1*r1*r2); //primeiro ajusta a altura, depois o pitch e por fim o roll da cloud voxelized e guarda em cloud_rotated
    std::cout<< "Going to log rotated cloud" << std::endl;
    //13. Guarda a nuvem em ficheiro
    logCloud(cloud_rotated,CloudInAnalysis + "_rotated", CloudInAnalysis + " rotated according to the camera pose");

}

// Applies outlier removal from a cloud using statistical outlier removal. 
// We opt by this method because the density of the clouds is quite homogenous
void DetectionHelper::cleanPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clean){

    //float stdDevThresh= estimateStdDevDistance(cloud, 50);

    //The calculation of std dev does not serve, so maybe we need to calculate it for the whole cloud?
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor;
    sor.setInputCloud (cloud);
    sor.setMeanK (30);
    sor.setStddevMulThresh (2); // We attempt different values and this was the best.
    sor.filter (*cloud_clean);

    std::cout << "Point cloud clean report: Original cloud size=" << cloud->points.size() << "; Clean cloud size=" << cloud_clean->points.size();
    std::cout << "; cleaned " << cloud->points.size() - cloud_clean->points.size()<< " points" << std::endl; 

    logCloud(cloud_clean,CloudInAnalysis+"_clean", CloudInAnalysis + " cleaned from outliers");

}


// Applies a cloud extraction based on indexes
// if negative is true gets all points that don't belong to the inliers indexes
void DetectionHelper::extractCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_extracted, pcl::PointIndices::Ptr inliers, bool negative){
    //Initializes the index extractor, i.e., an object responsible to extract a point cloud based on a given set of indexes
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    extract.setInputCloud(cloud_in);
    extract.setIndices(inliers);
    extract.setNegative(negative);
    extract.filter(*cloud_extracted);
}

void DetectionHelper::getPointsAboveFloor(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_above_floor, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane){
    // Saves the coefficients that best describes the plane equation (ax+by+cz+d)
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    // Saves the indexes of the points that belong to that plane
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
    // Gets coeffs and inliers by applying RANSAC
    //getDominantPlane(seg, cloud_in, coefficients, inliers, 0.015d);

    //In sequence 2 the wall is bigger than the floor, and we don't want to detect it as floor.
    //For that reason we are going to clip first a part of the cloud that for sure has the floor, to "make the wall smaller"
    //We are assuming that this function is called when cloud_in is rotated
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clipped (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clipped_negative (new pcl::PointCloud<pcl::PointXYZRGBA>);

    pcl::PassThrough<pcl::PointXYZRGBA> pass;
    pass.setInputCloud (cloud_in);
    pass.setFilterFieldName ("y");
    pass.setFilterLimitsNegative(false);
    pass.setFilterLimits (-0.3, 0.3);
    pass.filter (*cloud_clipped);

    

    //==FLOOR EXTRACTION
    // Fround the cloud clipped we get the dominante plane
    getDominantPlane(seg, cloud_clipped, coefficients, inliers, 0.017d);
    // Gets the cloud belonging to the floor. Just for logging purposes
    // Extracts the cloud that belongs to the inliers index set (floor)
    extractCloud(cloud_clipped, cloud_on_plane, inliers, false);


    //==ABOVE FLOOR EXTRACTION
    // Extracts the negative of the clipping, which is part of the cloud above floor
    pass.setFilterLimitsNegative(true);
    pass.filter (*cloud_clipped_negative);

    // Extracts the cloud that belongs above the floor inside the cloud clipped
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clipped_above_floor (new pcl::PointCloud<pcl::PointXYZRGBA>);
    extractCloud(cloud_clipped, cloud_clipped_above_floor, inliers, true);
    // this portion of cloud above floor inside cloud clipped can still have some


    mergeClouds(cloud_clipped_negative, cloud_clipped_above_floor);

    //==FLOOR CHUNK FILTERING
    //Filters floor chunks from cloud above floor
    pass.setInputCloud (cloud_clipped_negative);
    pass.setFilterLimitsNegative(true);
    pass.setFilterLimits (-0.02, 0.3);
    pass.filter (*cloud_above_floor);
    //And readds them to the cloud on floor
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr floor_chunks (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pass.setFilterLimitsNegative(false); 
    pass.filter (*floor_chunks);

    mergeClouds(cloud_on_plane,floor_chunks);

    logCloud(cloud_on_plane, CloudInAnalysis + "_extracted_floor", CloudInAnalysis + " RANSAC extracted floor");

    logCloud(cloud_above_floor, CloudInAnalysis + "_above_floor", CloudInAnalysis + " RANSAC extracted cloud above floor");


}



// Merges cloud2 into cloud1, so cloud1 will be cloud1+cloud2
void DetectionHelper::mergeClouds(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud1, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud2){

    for(int i = 0; i<cloud2->size(); i++){
        cloud1->points.push_back(cloud2->points[i]);
    }
  //  cloud1->points.insert(cloud1->points.end, cloud2->points.begin(), cloud2->points.end());
    // For some reason we must redifine this, or when saving the cloud an error is thrown
    cloud1->width = cloud1->points.size();
    cloud1->height = 1;
    cloud1->is_dense = true; // this is kind of cheating, but we are saying there are no invalid points like inf or nan, but how it is guarantee? it is recommend to have the cloud voxelised before
}


// Applies a clustering operation into cloud, and classifies each object
std::vector<DetectionHelper::ClassifiedObject> DetectionHelper::extractObjectsFromCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in){
    // This vector will save the objects classified
    std::vector<ClassifiedObject> classifiedObjects = std::vector<ClassifiedObject>();
    easyLog("Input cloud to extract object with size " + std::to_string(cloud_in->points.size()));
    // First we divide cloud into clusters
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud (cloud_in);
    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;

    ec.setClusterTolerance (0.02);
    ec.setMinClusterSize (25);  // originally setup to 10, but then we got a cluster of 12 points 
    //ec.setMaxClusterSize (25000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud_in);
    ec.extract (cluster_indices);
    
    // Then we analyse each cloud
        
    // We first create the clouds and save them to a vector
    int max_size = 0;
    int biggest_cloud_index = 0;
    std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> cluster_clouds = std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr>();
    for(int i=0; i<cluster_indices.size(); i++){
        // Gets the i cluster index array
        pcl::PointIndices indexes = cluster_indices[i];
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
        // Creates a cloud from the indices
        copyToCloud(cloud_in, cluster_cloud, indexes);
        std::cout << "Cluster " << i << " with size " << cluster_cloud->points.size() << std::endl;
        // Saves the cloud
        logCloud(cluster_cloud, CloudInAnalysis + "_cluster_" + std::to_string(i), "corresponding to a cluster extracted from a subset of points from " + CloudInAnalysis);               
        
        // Pushes cloud into vector
        cluster_clouds.push_back(cluster_cloud);

        // This helps registering the biggest cloud
        if(cluster_cloud->size() > max_size){ // Note to self: I just discovered this was unecessary, because clusterization orders the clouds from biggest to smallest :| 
                                              //               They don't document it very clearly. I went to see the source code to confirm: http://docs.ros.org/hydro/api/pcl/html/extract__clusters_8hpp_source.html
            max_size = cluster_cloud->size();
            biggest_cloud_index = cluster_clouds.size()-1; //we could just use "i" but this guarantees it
        }
    }

    // ------- CLASSIFICATION --------

    // First we deal with the walls
    // This piece of code is being prepared for a scenario were different walls could be separated into different clusters
    // We don't define a fixed trigger because it may vary depending on the voxel leaf size, that can be changed anytime
    // So we know that for sure the biggest cluster is a wall. By our experiences made for sequence1, and for different voxel leaf sizes
    // The wall cloud can be 11 to 12 times bigger (in points) than the seconds biggest (in points) cloud (the chair)
    // So a point size threshold can be defined as wall size / 6. We can change this value latter on

    // ATENÇÃO: MUDAR ISTO PARA CONSIDERAR ANTES A ALTURA DAS PAREDES PAH!

    // ---------- WALL CLASSIFICATION -----------------
    //int wallMinSizeThreshold = cluster_clouds[biggest_cloud_index]->size()/6; // TODO: Consider using wall width and height instead of number of points. In a bigger room this would be a problem, because depth resolution decreases with distance
                                                                              // Maybe we should consider only using height, 
    // Grabs the min and max points of a wall
    pcl::PointXYZRGBA wallMinPt, wallMaxPt;
    pcl::getMinMax3D (*cluster_clouds[biggest_cloud_index], wallMinPt, wallMaxPt);
    double wallHeight = abs(wallMinPt.y-wallMaxPt.y);
    double wallMinHeightThreshold = 0.80*wallHeight; //Minimal height an object must have to be considered a wall.
    int wallMinPointThreshold = cluster_clouds[biggest_cloud_index]->size()/8; // Minimal number of points for an object to be considered as a wall
    int wallCount = 0;
    // We get all the clouds that are for sure walls
    std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> wall_clouds = std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr>();
    std::vector<int> wallIndexesInCluster = std::vector<int>(); // We save on this vector all the cluster indexes we shall exclude (because they will be considered walls)

    for(int i=0; i<cluster_clouds.size(); i++){
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster_cloud = cluster_clouds[i];

        // Grabs the min and max points of the cluster
        pcl::PointXYZRGBA clusterMinPt, clusterMaxPt;
        pcl::getMinMax3D (*cluster_cloud, clusterMinPt, clusterMaxPt);
        double clusterHeight = abs(clusterMinPt.y-clusterMaxPt.y);

        // If this cluster has a size bigger than the treshold, it is a wall
        if(cluster_cloud->size() >= wallMinPointThreshold && clusterHeight >  wallMinHeightThreshold){
            wallIndexesInCluster.push_back(i); //This cluster is for sure a wall
            wall_clouds.push_back(cluster_cloud);
            
            std::cout<< "Cluster " <<  i  << " extracted from cloud " << CloudInAnalysis << " is a wall" << std::endl;
            std::cout << "\t Going to check if other clusters shall belong inside the wall. This may take some time" << std::endl;
            // Now that we have a wall, we are going to do multiple RANSACs on it
            // A wall can have an L-SHAPE, and we need to separate them into different clusters
            // Also wall pieces cannot be classified as being a normal object, so we will concatenate those
            bool stop = false;
            // This cloud will save portions of the wall cloud. After each iteration shall have a cloud without the RANSAC extracted cloud
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr temp_cloud = cluster_cloud;
            // In each iteration of this while loop, we call ransac
            int k=0;
            while(!stop){
                // Let's RANSAC the wall!
                double a,b,c,d; // coeffs of plane equation
                pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
                pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
                pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
                //Gets coeffs and inliers by applying RANSAC
                getDominantPlane(seg, temp_cloud, coefficients, inliers);
                normalizePlaneCoeffs(coefficients, &a,&b,&c,&d);

                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane (new pcl::PointCloud<pcl::PointXYZRGBA>);
                // Extracts the biggest piece of wall
                extractCloud(temp_cloud, cloud_on_plane, inliers, false);
                // Extracts the rest of the wall for the next iteration
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_off_plane (new pcl::PointCloud<pcl::PointXYZRGBA>);
                extractCloud(temp_cloud, cloud_off_plane, inliers, true);

                // We get the max and min of the wall in analysis
                pcl::PointXYZRGBA planeMinPt, planeMaxPt;
                pcl::getMinMax3D(*cloud_on_plane, planeMinPt, planeMaxPt); 
                
                std::cout<< "Wall coeffs " << " a=" << a << ", b="<< b << ", c=" << c << ", d=" << d << std::endl;

                double planeHeight = abs(planeMinPt.y-planeMaxPt.y);

                if(cloud_on_plane->size() >= wallMinPointThreshold && planeHeight > wallMinHeightThreshold){
                    
                    // Wall chunk detection
                    // Now lets loop into all the clusters that we detected before, like chair, robot, etc (except the cloud_clusters[i], because we don't want to fuse the wall with itself :) )
                    // this loop will check if any of the clusters belong the wall
                    for(int j=0; j<cluster_clouds.size(); j++) {
                        if(i!=j){ // Again, we are not going to make the bounding box analysis comparing the same object
                            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr potential_wall_piece = cluster_clouds[j];
                            pcl::PointXYZRGBA potMinPt, potMaxPt;
                            pcl::getMinMax3D(*potential_wall_piece, potMinPt, potMaxPt); 
                            double potHeight = abs(potMinPt.y-potMaxPt.y);
                            // This verification won't affect the veracity of the following logic but
                            // we don't want to compare to clusters that are for sure walls. We just want to check the other objects which we are not quite sure :)
                            // i.e., we are guaranteeing that we are just analysing a potential wall piece, that can be a wall piece or an object (thats what we want to check)
                            if(potential_wall_piece->size() < wallMinPointThreshold || potHeight < wallMinHeightThreshold){
                                std::cout << "Going to check if this is a wall piece " << std::endl;
                                // To verify if this is a wall piece, we are going to use the coeffs to evaluate a cloud belongs to a plane
                                // the tolerance of 0.03 was estimated manually. I applyed the plane eq to the points inside of different objects. 
                                // The closest object in seq 1, plane eq resulted in a value of 0.5 (aprox) while the wall pieces in 0.01(aprox).
                                // Even if an object is too close to the wall, we determine that at least 40% of it must belong to the wall, which is not the case
                                // Although, if we face wider objects close to the wall, consider using a bigger percentage
                                bool isWallPiece = belongsToPlane(potential_wall_piece, a,b,c,d, 0.03, 0.4);
                                if(isWallPiece){
                                    wallIndexesInCluster.push_back(j); //This cluster is classified as a wall piece, so it goes out from the original cluster vector
                                    //Then we save it for logging purposes. i will stand for the cluster containing a big wall, k for a surface got from ransac 
                                    //from this wall, and j for the cluster index corresponding to the piece of wall
                                    logCloud(potential_wall_piece, CloudInAnalysis+"_wallpiece_"+std::to_string(i)+"_"+std::to_string(k)+"_"+std::to_string(j), CloudInAnalysis+"_wallpiece_"+std::to_string(i)+"_"+std::to_string(k)+"_"+std::to_string(j));
                                    //Now lets merge the walls. This puts all the points inside potential_wall_piece inside the cloud_on_plane
                                    mergeClouds(cloud_on_plane, potential_wall_piece);
                                }
                            }

                        }
                    }

                    //Now that we have a complete wall (or hope so), we classify the object
                    pcl::PointXYZRGBA wallMinPt, wallMaxPt, wallCenter;
                    pcl::getMinMax3D (*cloud_on_plane, wallMinPt, wallMaxPt);
                    double wallHeight = abs(wallMinPt.y-wallMaxPt.y);
                    double wallWidth = findXoZMaxDistance(cloud_on_plane, CloudInAnalysis+"_wall_"+std::to_string(i)+"_"+std::to_string(k)+"_projected", 4);
                    estimateCenterOfMass(cloud_on_plane, wallCenter);
                    ClassifiedObject classifiedObject;
                    classifiedObject.minPt = wallMinPt;
                    classifiedObject.maxPt = wallMaxPt;
                    classifiedObject.centerOfMassAndMeanColor=wallCenter;
                    classifiedObject.width = wallWidth;
                    classifiedObject.height = wallHeight;
                    classifiedObject.cloud = cloud_on_plane;
                    classifiedObject.name = "wall_"+std::to_string(wallCount);
                    classifiedObject.classification = ObjectClassification::WALL;
                    classifiedObjects.push_back(classifiedObject);

                    std::cout << "Surface "  << k << " from cluster " << i << " is classified as WALL" << std::endl;
                    wall_clouds.push_back(cloud_on_plane);

                    logCloud(cloud_on_plane, CloudInAnalysis+"_wall_"+std::to_string(i)+"_"+std::to_string(k), "containing a wall after merging with wall pieces");
                    wallCount++;
                }
                else{
                    stop = true;
                }



                // This prevents analysing add infinitum on small chunks of wall
                // RANSAC gets the dominant surface, but won't extract the whole surface, even if it visually belong. So we are going to ignore those "wild points"
                pcl::PointXYZRGBA offMinPt, offMaxPt, offCenter;
                pcl::getMinMax3D (*cloud_off_plane, offMinPt, offMaxPt);
                double offHeight = abs(offMinPt.y-offMaxPt.y);
                if(cloud_off_plane->size() < wallMinPointThreshold && offHeight > wallMinHeightThreshold){
                    stop = true;

                }

                temp_cloud = cloud_off_plane;
                k++;
            }
           
        }
    }

    // ----------- OBJECT CLASSIFICATION--------------
    // At this point we have classified the walls
    // Now we need to solve another problem. Other objects can have separated chunks
    // For example, the chair in seq 1 has its legs separated
    // For this we are going to do the following:
    //     -Get the object cloud
    //     -Determine its bounding cylinder 
    //     -Checks for the closest objects (we can use a center of mass and radius)
    //     -merges and saves the merged index
    //     -registers a cluster to remove

    // We save on this vector all the cluster indexes that correspond to clouds that contain pieces of an object
    // For example, in seq 1, legs of the chair are separated from the rest of the chair. Those clouds will be present here
    std::vector<int> objectPiecesInCluster = std::vector<int>(); 

    int robotCount = 0;
    int otherCount = 0;
    for(int i=0; i<cluster_clouds.size(); i++ ){
        //First we check if it is not a cluster belonging to the wall, or if not already excluded in a previous iteration
        if(!(std::find(wallIndexesInCluster.begin(),wallIndexesInCluster.end(),i)!=wallIndexesInCluster.end())
            && !(std::find(objectPiecesInCluster.begin(),objectPiecesInCluster.end(),i)!=objectPiecesInCluster.end())){

            // Then the first phase of classification of an object as other or robot is to merge pieces of objects    
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr object_cloud = cluster_clouds[i];
            // Very important note: since the cloud is upside down, the min and max points shall be considered in "reverse".
            pcl::PointXYZRGBA minPt, maxPt, centerPt, topSurfaceCenterPt;
            pcl::getMinMax3D (*object_cloud, minPt, maxPt);
            //Note: objects may not have their cloud complete. So we set maxPt.y to 0. We do that to maxPt and not minPt, because the cloud is oriented to -y
            double centerOfMassVerticalShift = maxPt.y; //it will be always negative
            maxPt.y=0; // since the object cloud can have missing points on its base, we set its minimal point to 0
            centerPt.y -= centerOfMassVerticalShift;
            std::cout << "Applied to cluster " << i << " center of mass a vertical shift of " << centerOfMassVerticalShift << " on the y channel. ";
            std::cout << "The shift only applies to the computed center of mass value. Won't affect the cloud points." << std::endl;

            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr top_surface_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
            bool shiftedCM =false;

            // The height of an object is easy. We are assuming that it is always vertical, so, we just use abs(ymax-ymin)
            double maxHeight = abs(maxPt.y-minPt.y);

            // The width is more complicated. We need to project an object in XoZ and find the biggest distance, but we also must consider objects can be incomplete
            // Computes the max width. Uses an extended version of it. This can be useful for the case of the chair. It is able to do it based
            // on the top surface. When the top surface is too small (for example, the case of the statue), the default XoZ projection will be considered
            // In the case we have a relevant top surface with a valid width value, shiftedCM will be set to true, and we shall use top_surface_cloud to determine a temp center of mass
            double maxWidth = findXoZMaxDistance(object_cloud,top_surface_cloud, CloudInAnalysis+"_cluster_" + std::to_string(i)+ "_projected", 5, true,0.7,  shiftedCM);

            // We previously had this on the next if, but since we are needing the value of topSurfaceCenterPt to determine if the object is a robot
            // we moved this line to here. This topSurfaceCenterPt shall be used with precaution. It is intended to be only used when an object
            // has a shifted center of mass. In the case of the robot, top_surface_cloud will be for sure valid for use, and we only need it for height estimation based only on its surface
            // avoiding the impact of the objects above it. 
            // How would this verification act when it has a cat on its top? :)
            estimateCenterOfMass(top_surface_cloud, topSurfaceCenterPt);

            //pcl::compute3DCentroid(object_cloud, &centerPt);
            estimateCenterOfMass(object_cloud, centerPt);
            if(shiftedCM){ // In the case of a shiftedCM, we recenter it in XOZ
                std::cout << "Object top surface cloud with " << top_surface_cloud->size()<< std::endl;
                centerPt.x = topSurfaceCenterPt.x;
                centerPt.z = topSurfaceCenterPt.z;
            }

            if(top_surface_cloud->size()>0)
                logCloud(top_surface_cloud, CloudInAnalysis+"_cluster_"+std::to_string(i)+"_top_surface", " that contains top surface of cluster "+std::to_string(i));

            std::cout << "Computed min, max and estimated center of mass (with mean colour) for cluster " << i << std::endl;
            logMinMaxCenter(minPt, maxPt, centerPt);
            
            // The height of an object is easy. We are assuming that it is always vertical, so, we just use abs(y)
            // The width is more complicated. We need to project an object in XoZ and find the biggest distance. Maybe we shall use KdTree
            //double maxWidth = findXoZMaxDistance(object_cloud, CloudInAnalysis+"_cluster_" + std::to_string(i)+ "_projected", 5);
            //double maxHeight = abs(maxPt.y-minPt.y);
            std::cout << "Max width of cluster " << i << " is " << maxWidth << std::endl;
            std::cout << "Max height of cluster " << i << " is " << maxHeight << std::endl;
            

            // Now we need to merge clusters that may belong to this one
            // for example, the chair legs
            // To determine that we go through all other clusters that are not walls and are not this own cluster
            // and check if more than a percentage of points belong to the bounding cilinder of this cluster (plus a tolerance)
            bool objectUpdated = false; // will be true when a object has a merge update
            for(int j=0; j<cluster_clouds.size();j++){
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr potential_piece_cloud = cluster_clouds[j];
                // Again, we exclude from consideration the walls, and chunks that were already excluded in previous iterations
                // we also avoid analysing against itself
                if( potential_piece_cloud->size() < object_cloud->size() // To spare cicles. We want to merge to this object pieces that we assume are smaller
                    &&i!=j 
                    && !(std::find(wallIndexesInCluster.begin(),wallIndexesInCluster.end(),j)!=wallIndexesInCluster.end())
                    && !(std::find(objectPiecesInCluster.begin(),objectPiecesInCluster.end(),j)!=objectPiecesInCluster.end())
                ){  
                    // Now we check if cluster_clouds[j] is withing the bounding cylinder of cluster_clouds[i]
                    std::cout<< "Going to evaluate if cluster " << j << " of size " << potential_piece_cloud->size() << " belongs to cluster " << i << " of size " << object_cloud->size() << std::endl;
                    
                    bool isPiece = cloudWithinBoundingCylinder(potential_piece_cloud, centerPt, maxWidth/2, maxHeight, 0.20, 0.20, false); 
                    if(isPiece){
                        objectUpdated = true;
                        std::cout<< "Cluster " << j << " is a piece belonging to cluster " << i << std::endl;
                        objectPiecesInCluster.push_back(j);
                        mergeClouds(object_cloud, potential_piece_cloud);
                        logCloud(object_cloud, CloudInAnalysis+"_cluster_"+std::to_string(i)+"_merged", ("resulting from a merge between cluster" +std::to_string(i) + " and cluster " + std::to_string(j)));
                    }
                }
            }

            // Since the object can be updated with new points, we need to recompute its center of mass
            // We recompute maxWidth anyway because the extended version will for sure give smaller values.
            double previousMaxWidth = maxWidth; //To compute if we are in a presence of a robot, we consider the max width got by the extended findXoZMaxDistance.
                                                //For the case of the robot it will give a more precise value
            maxWidth = findXoZMaxDistance(object_cloud,CloudInAnalysis+"_cluster_" + std::to_string(i)+ "_projected_2",5);
            if(objectUpdated){
                pcl::getMinMax3D (*object_cloud, minPt, maxPt);
                centerOfMassVerticalShift = maxPt.y;
                estimateCenterOfMass(object_cloud, centerPt);
                centerPt.y -= centerOfMassVerticalShift;
            }

            // Now that we have this object merged with wild parts of it we need to classify if an object is a robot or other
            ClassifiedObject classifiedObject;
            classifiedObject.minPt = minPt;
            classifiedObject.maxPt = maxPt;
            classifiedObject.centerOfMassAndMeanColor=centerPt;
            classifiedObject.width = maxWidth;
            classifiedObject.height = maxHeight;
            classifiedObject.cloud = object_cloud;

            // top_surface_cloud
            // Initially we attempted to find if it was a robot by just reading the maxHeight got from the y distance between min and max points 
            // That height is only valid for "OTHER" objects, but robot has a sensor and volumetric marker that increases is height
            // So for this we must look at top_surface_cloud.

           /* std::cout << "\n\n TO CHECK IF CLUSTER " << i << " IS A ROBOT WE CONSIDER HEIGHT = " << abs(topSurfaceCenterPt.y) << " AND MAX WIDTH = " << maxWidth  << std::endl;
            std::cout << " TO CHECK IF CLUSTER " << i << " IS A ROBOT WE CONSIDER HEIGHT = " << abs(topSurfaceCenterPt.y) << " AND PREVIOUS MAX WIDTH = " << maxWidth << std::endl;
            std::cout << "\n\n Is robot verification 1 " << isRobot(maxWidth, abs(topSurfaceCenterPt.y)) << std::endl;
            std::cout << "\n\n Is robot verification 2 " << isRobot(previousMaxWidth, abs(topSurfaceCenterPt.y)) << "\n\n"<< std::endl;*/

            // top_surface_cloud, may be empty. So first we evaluate if at least the is a width determined (its computation is more secure to fails)
            // Then we determine again the height, by determining again the top surface.
            double heightForRobotClassification = abs(topSurfaceCenterPt.y);
            if((isPotentialRobot(maxWidth) || isPotentialRobot(previousMaxWidth)) && top_surface_cloud->size()==0){
                std::cout << "Cluster " << i << " is a potential ROBOT" << std::endl;

                pcl::ModelCoefficients::Ptr prCoeffs (new pcl::ModelCoefficients ());
                pcl::PointIndices::Ptr prInliers (new pcl::PointIndices ());
                pcl::SACSegmentation<pcl::PointXYZRGBA> prSeg;
                //Gets coeffs and inliers by applying RANSAC
                getDominantPlane(prSeg, object_cloud, prCoeffs, prInliers, 0.005);       
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pr_surface (new pcl::PointCloud<pcl::PointXYZRGBA>);
                // Extracts the surface
                extractCloud(object_cloud, pr_surface, prInliers, false); 

                pcl::PointXYZRGBA prCenterPt;
                estimateCenterOfMass(pr_surface,prCenterPt);

                heightForRobotClassification = abs(prCenterPt.y);
            }

            std::cout << "\n\n TO CHECK IF CLUSTER " << i << " IS A ROBOT WE CONSIDER HEIGHT = " << heightForRobotClassification << " AND MAX WIDTH = " << maxWidth  << std::endl;
            std::cout << " TO CHECK IF CLUSTER " << i << " IS A ROBOT WE CONSIDER HEIGHT = " << heightForRobotClassification << " AND PREVIOUS MAX WIDTH = " << maxWidth << std::endl;
            std::cout << "\n\n Is robot verification 1 " << isRobot(maxWidth, heightForRobotClassification) << std::endl;
            std::cout << "\n\n Is robot verification 2 " << isRobot(previousMaxWidth, heightForRobotClassification) << "\n\n"<< std::endl;



            if(isRobot(maxWidth, heightForRobotClassification)||isRobot(previousMaxWidth, heightForRobotClassification)){
                std::cout << "Cluster " << i << " is classified as a ROBOT" << std::endl;
                classifiedObject.classification = ObjectClassification::ROBOT;
                classifiedObject.name="robot_"+std::to_string(robotCount);
                // Classifies objects on top of robot (Laser and/or marker)
                classifiedObject.childObjects = extractRobotTopElements(classifiedObject.cloud, robotCount);
                robotCount++;
            }
            else{
                std::cout << "Cluster " << i << " is classified as OTHER" << std::endl;
                classifiedObject.classification = ObjectClassification::OTHER;
                classifiedObject.name="other_"+std::to_string(otherCount);
                otherCount++;
            }
            classifiedObjects.push_back(classifiedObject);
        }
    }
    


    return classifiedObjects;
}


// Classifi
std::vector< DetectionHelper::ClassifiedObject> DetectionHelper::extractRobotTopElements(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr robotCloud, int robotNumber){

    std::vector<ClassifiedObject> objectsAboveRobot;

    // First we separate the cloud above the surface, based on what was teached on cloud clipping on lab 3
    // To do that we extract surface information, to know where to clip
    pcl::ModelCoefficients::Ptr rCoeffs (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr rInliers (new pcl::PointIndices ());
    pcl::SACSegmentation<pcl::PointXYZRGBA> rSeg;
    //Gets coeffs and inliers by applying RANSAC
    getDominantPlane(rSeg, robotCloud, rCoeffs, rInliers, 0.005);       
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr r_surface (new pcl::PointCloud<pcl::PointXYZRGBA>); // Surface of robot
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr r_off_surface (new pcl::PointCloud<pcl::PointXYZRGBA>); // Rest of the robot
    // Extracts the surface
    extractCloud(robotCloud, r_surface, rInliers, false); 
    // Extracts the objects that do not belong to the surface
    extractCloud(robotCloud, r_off_surface, rInliers, true); 

    pcl::PointXYZRGBA rSurfaceCenterPt;
    estimateCenterOfMass(r_surface, rSurfaceCenterPt);

    // Now we grab the cloud off robot surface and apply clipping
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr above_surface_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>); //Saves the objects above cloud
    pcl::PassThrough<pcl::PointXYZRGBA> pass;
    pass.setInputCloud (r_off_surface);
    pass.setFilterFieldName ("y"); // We want to clip on y axis
    pass.setFilterLimitsNegative(true); // We are not sure what is the safe max, so we clip from 0 to the surface center.y and get the rest of cloud
    pass.setFilterLimits (rSurfaceCenterPt.y-0.02, 0); // once again, rSurfaceCenterPt.y is negative
    pass.filter (*above_surface_cloud);
   
    logCloud(above_surface_cloud, CloudInAnalysis+"_robot_"+std::to_string(robotNumber)+"_above_cloud",CloudInAnalysis+"_robot_"+std::to_string(robotNumber)+"_above_cloud");

    // Then we clusterize the elements. Guarantee that we just end with two clusters!

    // First we divide cloud into clusters
    // TODO: We are using clustering in more than one place. If there is time, extract this to a function
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud (above_surface_cloud);
    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;

    ec.setClusterTolerance (0.02);
    ec.setMinClusterSize (15); 
    ec.setMaxClusterSize (100); // To prevent cat on the roomba :D
    ec.setSearchMethod (tree);
    ec.setInputCloud (above_surface_cloud);
    ec.extract (cluster_indices);

    // To make a distinction between what is the laser and what is the marker
    // the marker has a width/height ratio lower than the laser
    // We could use findXoZ max distance, but it is too expensive for this
    // So instead we just compute the height being maxY (or min since we are upside down) minus 16cm
    // and width as being he horizontal distance (we just set y to zero) between min and max points

    // We just want to analyse two clusters, and the bigger ones 
    int numIterations = cluster_indices.size()>2 ? 2 : cluster_indices.size();
    for(int i=0; i< numIterations; i++){
        // Gets the i cluster index array
        pcl::PointIndices indexes = cluster_indices[i];
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
        // Creates a cloud from the indices
        copyToCloud(above_surface_cloud, cluster_cloud, indexes);
        std::cout << "Object on top of robot with index (of object) " << i << " with size " << cluster_cloud->points.size() << std::endl;
 

        // It is a marker if it has a width/height ratio below 0.5
        pcl::PointXYZRGBA minPt, maxPt, centerPt; //although we call them min and max, we have to consider that the cloud is upside down
        pcl::getMinMax3D(*cluster_cloud, minPt, maxPt);
        estimateCenterOfMass(cluster_cloud, centerPt); // estimates the center of mass of the cluster
        // Since the clouds were cleaned prior to this, we must shift the min (I mean, max) y to the surface center y
        maxPt.y=rSurfaceCenterPt.y; // Shifts the lowest point of the cluster_cloud to level it with the surface.
        double height = abs(maxPt.y-minPt.y);
        // To determine the "pseudo" width we need to project the min and max points into XoZ, so we set y to 0
        pcl::PointXYZRGBA projectedMinPt=minPt, projectedMaxPt=maxPt;
        projectedMaxPt.y=0;
        projectedMinPt.y=0;
        double width = distanceBetweenPoints(projectedMaxPt, projectedMinPt);
        // Now we compute the ratio
        double widthHeightRatio = width/height;

        ClassifiedObject classifiedObject;
        classifiedObject.minPt = minPt;
        classifiedObject.maxPt = maxPt;
        classifiedObject.centerOfMassAndMeanColor=centerPt;
        classifiedObject.width = width;
        classifiedObject.height = height;
        classifiedObject.cloud = cluster_cloud;

        logCloud(cluster_cloud, CloudInAnalysis+"_robot_top_"+std::to_string(i), CloudInAnalysis+"_robot_top_"+std::to_string(i));

        // Initially we used a ratio between width and height to check if it is a marker or a laser
        // but that can fail. If the height is bigger that width, for sure we have a marker

        //if( widthHeightRatio < 0.5){ //is a marker
        if( height > width){ //is a marker
            classifiedObject.classification = ObjectClassification::VOLUMETRIC_MARKER;
            classifiedObject.name="robot_"+std::to_string(robotNumber)+"_marker";
        }
        else{ // is a laser
            classifiedObject.classification = ObjectClassification::LASER;
            classifiedObject.name="robot_"+std::to_string(robotNumber)+"_laser";
        }
        objectsAboveRobot.push_back(classifiedObject);

    }

    // TODO: Na cloud 9 e 10 da seq 1 o marcador não é detectado. Optar por ou reduzir a voxelização, ou a limpeza, sacrificando a performance, 
    //       ou então, se calhar optar por dar maior tolerância à clusterização e implementar a classificação por critérios, tal como especificado no comentário abaixo em português
    //       Ou seja, para um objecto ser laser, tem de ser pelo menos duas destas métricas: ser mais largo, ser mais alto, ser mais denso

    // Second verification to protect against having two markers or two lasers
    // We don't add a more refined classification in the previous loop, because
    // there may be cases were there will be just one, so we only check for the objects self caracteristics without comparing to the other
    // THis second verification compares one object with the other (when we have two), in situations where two were classified the same
    if(objectsAboveRobot.size()>1 && objectsAboveRobot[0].classification == objectsAboveRobot[1].classification){
        
        // saves the index with the biggest value
        // One object is a laser if it is lower than the other, and wider
        bool isIndex1Laser = objectsAboveRobot[1].height < objectsAboveRobot[0].height && objectsAboveRobot[1].width > objectsAboveRobot[0].width;
        int laserIndex = isIndex1Laser;
        int markerIndex = !isIndex1Laser;

        // Outras métricas, densidade de pontos. separar antes por 3 métricas: largura, altura, densidade de pontos. pelo menos duas determinam o laser

        objectsAboveRobot[markerIndex].classification = ObjectClassification::VOLUMETRIC_MARKER;
        objectsAboveRobot[markerIndex].name="robot_"+std::to_string(robotNumber)+"_marker";

        objectsAboveRobot[laserIndex].classification = ObjectClassification::LASER;
        objectsAboveRobot[laserIndex].name="robot_"+std::to_string(robotNumber)+"_laser";

    }

    // Logs the clouds
    for(int i=0; i< objectsAboveRobot.size();i++)
        logCloud(objectsAboveRobot[i].cloud, CloudInAnalysis+"_"+objectsAboveRobot[i].name, objectsAboveRobot[i].name);

    // add the logcloud here and remove from previous loop


    // TODO: REFINAR A DETECÇÃO. "aumentar" a altura da superficie do robo. Fazer logs das clouds potenciais para ver o que está a falhar

    return objectsAboveRobot;

}