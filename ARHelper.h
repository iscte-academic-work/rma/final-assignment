 /* Mixed Reality and Applications
 *
 * Abel Tenera, Gilberto Junior, Samatha Cane
 *  2019-2020 ISCTE-IUL
 *
 *  Assumed frames of reference:
 *  - PCL: z-blue (into the scene), x-red (to the right), y-green (downwards)            meters
 *  - OSG: z (upwards), x (to the left), y (out of the scene)                            centimeters
 * 
 *  Conversion
 *  PCL to OSG
 *  X      -X div 100.0 
 *  Y      -Z div 100.0
 *  Z      -Y div 100.0 
 */
#include <osg/Camera>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>
#include <osg/Texture2D>
#include <osg/MatrixTransform>
#include <osg/GraphicsContext>
#include <osg/Depth>
#include <osg/Material>
#include <osg/Texture2D>
#include <osg/PositionAttitudeTransform>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/AlphaFunc>
#include <osg/ShapeDrawable>
#include <osgText/Font>
#include <osgText/Text>

#include <algorithm>
#include <sstream>
#include <fstream>  
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector> 
#include <iostream>
#include <math.h>  
#include <sstream>

#include "DetectionHelper.h"
#include "MarkerController.h"


/* This class is mainly responsible for the first two requirements from the assignment. Provides helper
*  functions to serve the need for detection
*/
class ARHelper{
public:
    const double PCL_TO_OSG_SCALE_FACTOR = 100.0;
    const std::string ARROW_MODEL_PATH = "../Resources/arrow.obj";
    const double viewPortHeight = 480.0;
    const double viewPortWidth = 640.0;

    static const char* textureVertexSource;

    static const char* phongFragmentSource;

    static const char* textureFramentSource;

    static const char* phongVertexSource;

    static const char* markerVertexSource;

    static const char* markerFragmentSource;
    
    // Represents cloud data
    unsigned char* data; // Color data
    unsigned char* dataDepth; // Depth data
    unsigned char* dataRiskColor; // Color data related to risk of collision to robot
    unsigned char* dataShadingFromRobot; // Color data related to risk of collision to robot

    // this values will be used to set perspective camera pitch, roll and height
    // they must be the same as the ones obtained when determining plane rotation
    double pitch, roll, height;
    
    // This are the cameras that will be used in scene
    osg::ref_ptr<osg::Camera> orthoCam; // Will look to an ortho texture (with depth info) created from the front view of the scene
    osg::ref_ptr<osg::Camera> perspectiveCam; // Will look to the real world in the same perspective as the real sensor. Will be father to the objects we add into scene

    // Saves a list of classified objects
    //std::vector<DetectionHelper::ClassifiedObject> classifiedObjects;

    // Saves buffer of classified objects lists. Each index of this buffer corresponds to a list of classified objects on cloud_[indexnumber+1]
    // So classifiedObjectsBuffer[0] will have the objects of cloud_1.pcd, classifiedObjectsBuffer[1] of the cloud_2.pcd
    // This also means that the last index contains the objects classified for the current cloud
    std::vector<std::vector<DetectionHelper::ClassifiedObject>> classifiedObjectsBuffer;

    // Saves the cloud in analysis.
    // Note to self: This will be the original cloud without any cleaning
    //              I questioned myself why, but after analysing Lab5 with attention, there will be a graph scene
    //              and the cameras of the graph scene will be rotated according to the pitch, roll and height obtained.
    //              So every object we add will be child of the perspective camera, so no problem. It is just a matter of conversion between PCL and OSG refs.
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud;

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr clean_cloud;

    DetectionHelper* detectionHelper;
    MarkerController* markerController;


    ARHelper(double pitch, double roll, double height);
    ~ARHelper();

    double aspectRatio(){
        return viewPortWidth/viewPortHeight;
    }

    void runARExperiment();
    
    void setCurrentCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr clean_cloud, std::vector<DetectionHelper::ClassifiedObject> classifiedObjects);


private:
    float rulerLength=2, rulerWidth=5;
    osg::ref_ptr<osgText::Text> distanceTextHudFrame;
    osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf;
    osg::ref_ptr<osg::PositionAttitudeTransform> shadowTransf;
    osg::ref_ptr<MarkerController> ctrler;
    bool collidedLeft=false,collidedRight=false,collidedFront=false,collidedBack=false,collidedBelow=false, moved=false;
    ARImageModes arImageMode = ARImageModes::NORMAL;
    osg::ref_ptr<osg::Geode> ruler;


    // From lab 5. Iterates through every point in the cloud and extacts color and depth data to different arrays
    void extractPointCloudColorDepthData(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, unsigned char* data, unsigned char* dataDepth);

    // Extracts a color array from the point cloud, expressing the risk of collision between robot and rest of the cloud
    void extractPointCloudColorCollisionRiskData(unsigned char* data);

    // Extracts a color array from the point cloud, expressing the risk of collision between robot and rest of the cloud
    void extractPointCloudColorShadingFromRobot(unsigned char* data);

    // From lab 5. Creates an ortho texture from color and depth data vectors, with a given width and height
    //              To be further used for orthoCam.
    //              Also returns the colour image to comute later between different modes
    void createOrthoTexture(osg::ref_ptr<osg::Geode> orthoTextureGeode, osg::Image *image , unsigned char* data, unsigned char* dataDepth, int width, int height);

    // Defines virtual ortho and perspective cameras, aplying the pitch, roll and height transforms 
    void setVirtualCameras(osg::ref_ptr<osg::Camera> orthoCamera, osg::ref_ptr<osg::Camera> perspectiveCamera, osg::ref_ptr<osg::Geode> orthoTexture);

    void runViewer(osg::ref_ptr<osg::Group> rootNode);

    osg::ref_ptr<osg::Program> createPhongShaderProgram();

    osg::ref_ptr<osg::Node> loadVirtualObject(osg::Material *material, std::string objPath);

    osg::ref_ptr<osg::Node> loadVirtualObject(std::string objPath);

    // Sets an animation path for a jump animation.
    // time defines the time needed to complete a jump. maxHeight is the maxHeight the object is shifted in z
    osg::AnimationPath* createLoopJumpAnimationPath( float time, float maxHeight);

    // Loads arrows on the top of objects and returns a transform for the group of arrows. Returns the transforms for the group
    osg::ref_ptr<osg::PositionAttitudeTransform> loadArrowsOnObjects();


    // Returns the index of the robot in the current cloud classified objects. Will return -1 if does not find any :(
    int getRobotIndex();
    
    // Returns the index of the robot in a specific buffer index. Will return -1 if does not find any :(
    int getRobotIndex(int bufferIdx);

    // Loads danger circles bellow each "OTHER" object in the form of cilinders
    osg::ref_ptr<osg::PositionAttitudeTransform> loadDangerCircles(); 
    
    // Creates a cylinder
    osg::ref_ptr<osg::Geode> createCylinder(osg::Vec3 center, double radius, double height, double r, double g, double b, double a);

    // Creates a capsule. It will be rotated to be horizontal
    osg::ref_ptr<osg::Geode> createCapsule(osg::Vec3 center, double radius, double height, double r, double g, double b, double a);

    osg::Vec3 pclToOsg(pcl::PointXYZRGBA pt){
        return osg::Vec3(-pt.x*PCL_TO_OSG_SCALE_FACTOR, -pt.z*PCL_TO_OSG_SCALE_FACTOR, -pt.y*PCL_TO_OSG_SCALE_FACTOR);
    }

    // Gets the classified objects list of the current cloud, which are on the last index of buffer
    std::vector<DetectionHelper::ClassifiedObject> getCurrentCloudObjects(){
        int lastIndex = classifiedObjectsBuffer.size()-1;
        return classifiedObjectsBuffer[lastIndex];
    }

    // Predicts robot next step, based on it's "speed"
    //osg::ref_ptr<osg::Geode> predictRobotNextStep();

    // Goes through previous cloud analysis, and gets the average robot speed, based on the distance between each step
    // In realtime, this would turn into an exponentially expensive task, so in that case would be more responsible to consider only
    // the last K clouds. Since this is an academic experience with limited clouds, we will iterate over all clouds analysed 
    float averageRobotSpeed();

    // Gets, for the current analysis, the robot orientation
    // Returns false in case o insucess
    // the vector will be returned normalized, and z will be 0
    bool getRobotOrientation(osg::Vec3 &orientation);

    // Predicts the next step of the robot based on its average speed and orientation
    // z will be 0
    // Returns false in case o insucess
    bool predictRobotNextStep(osg::Vec3 &nextPosition, osg::Vec3 &currentOrientation, osg::Vec3 &currentPosition, osg::Vec3 &speedVector, float &yaw, float &speed);

    // Predicts the next position and loads a shape indicating it
    osg::ref_ptr<osg::PositionAttitudeTransform> loadNextPositionDisplay();  

    // Loads a display for last position. This is a bad name, because it will only do it
    // When the last position is at a distance bigger than the robot width
    osg::ref_ptr<osg::PositionAttitudeTransform> loadDisplayForLastPosition(bool showAlways);

    // Extracts the different robots got. The last index contains the robot from the current cloud
    std::vector<DetectionHelper::ClassifiedObject> robotsBuffer(bool &currentCloudHasRobot){
        currentCloudHasRobot = false;
        std::vector<DetectionHelper::ClassifiedObject> robotBuffer;
        for(int i=0; i<classifiedObjectsBuffer.size(); i++){
            int rbIndex = getRobotIndex(i);
            
            if(rbIndex >= 0){
                currentCloudHasRobot = classifiedObjectsBuffer.size()-1 == i;
                std::vector<DetectionHelper::ClassifiedObject> classifiedObjs = classifiedObjectsBuffer[i];
                DetectionHelper::ClassifiedObject robot = classifiedObjs[rbIndex];
                robotBuffer.push_back(robot);
            }
        }
        return robotBuffer;
    }


    // Creates a marker and respective shadow
    void CreateMarker(osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf,
                   osg::ref_ptr<osg::PositionAttitudeTransform> shadowTransf);

    // Detects collisions from marker
    void detectCollisions(osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf,
                      pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr kdtree,
                      bool *collidedLeft, bool *collidedRight, bool *collidedFront, bool *collidedBack, bool *collidedBelow);

    // Creates a trace path between marker and robot. Returns true if no obstacle was found
    bool traceMarkerPathToRobot(osg::ref_ptr<osg::PositionAttitudeTransform> markerTransf, osg::ref_ptr<osg::PositionAttitudeTransform> tracepathGroupTransf, float* distance);

    osg::ref_ptr<osg::Geode> createTextureGeode(float width, float length, const std::string &imagePath);

    void removeAllChildren(osg::ref_ptr<osg::PositionAttitudeTransform> transf);    

    osg::ref_ptr<osg::Camera> createHUD();

    osg::ref_ptr<osg::Camera> createHUDCamera( double left, double right, double bottom, double top);    
    
    osgText::Text* createText( const osg::Vec3& pos, const std::string& content, float size );
};


